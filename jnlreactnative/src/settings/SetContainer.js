import React, { PureComponent } from "react";
import { Text, View } from "react-native";

import { DEFAULT_DURATION, FadeView } from "../base/utils/FadeView";
import { SetDetailView } from "./SetDetailView";
import { SetUpdateView } from "./SetUpdateView";

export class SetContainer extends PureComponent {
  state = { viewName: "detailView" };
  // state = { viewName: "updateView" };

  handleViewChange = viewName => {
    this.setState({ viewName: "" });
    setTimeout(() => this.setState({ viewName }), DEFAULT_DURATION);
  };

  render() {
    const { viewName } = this.state;
    return (
      <View style={{ backgroundColor: "white", flex: 1 }}>
        <FadeView
          isVisible={viewName == "detailView"}
          style={{ height: "100%" }}
        >
          <SetDetailView
            handleViewChange={() => this.handleViewChange("updateView")}
          />
        </FadeView>
        <FadeView
          isVisible={viewName == "updateView"}
          style={{ height: "100%" }}
        >
          <SetUpdateView
            handleViewChange={() => this.handleViewChange("detailView")}
          />
        </FadeView>
      </View>
    );
  }
}
