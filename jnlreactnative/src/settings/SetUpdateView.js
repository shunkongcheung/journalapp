import React, { PureComponent } from "react";
import { Text, View } from "react-native";
import { injectIntl, intlShape } from "react-intl";
import PropTypes from "prop-types";
import * as yup from "yup";

import { FormView } from "../base/formView/FormView";
import { HOCFetchEdit } from "../base/fetches/HOCFetchEdit";
import { Loading } from "../base/utils/Loading";
import { PREFIX_UAMUSER } from "../base/fetches/actionPrefixes";
import { Title } from "../base/utils/Title";

export const SetUpdateView = HOCFetchEdit(
  injectIntl(
    class SetUpdateView extends PureComponent {
      static propTypes = {
        handleViewChange: PropTypes.func.isRequired,
        intl: intlShape.isRequired
      };
      state = { content: {}, options: {} };

      async componentDidMount() {
        const { fetchDetail, intl } = this.props;
        const { ok, content, options } = await fetchDetail(
          PREFIX_UAMUSER,
          "self"
        );
        options.password.type = "password";

        options.confirm_password = values => ({
          label: intl.formatMessage({ id: "setUpdateView.confirm_password" }),
          required: values.password,
          type: "password"
        });
        if (ok) this.setState({ content, options });
      }

      handleSubmit = async (data, { setErrors }) => {
        const { fetchSubmitUpdate, handleViewChange } = this.props;
        const { errors, ok } = await fetchSubmitUpdate(
          PREFIX_UAMUSER,
          "self",
          data
        );
        if (ok) handleViewChange();
        else setErrors(errors);
      };

      render() {
        const { content, options } = this.state;
        const { handleViewChange, intl } = this.props;

        return (
          <FormView
            handleBack={handleViewChange}
            handleSubmit={this.handleSubmit}
            initialValues={content}
            title={intl.formatMessage({ id: "setUpdateView.title" })}
            options={options}
            validationSchemaShape={{
              confirm_password: yup.mixed().when("password", {
                is: val => val,
                then: yup
                  .string()
                  .oneOf([yup.ref("password")])
                  .required(),
                otherwise: yup.mixed()
              })
            }}
          />
        );
      }
    }
  )
);
