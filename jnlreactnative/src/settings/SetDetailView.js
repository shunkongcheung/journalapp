import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Text, View } from "react-native";
import { Button } from "react-native-elements";
import { injectIntl, intlShape } from "react-intl";
import PropTypes from "prop-types";

import { Loading } from "../base/utils/Loading";
import { HOCFetchEdit } from "../base/fetches/HOCFetchEdit";
import { PREFIX_UAMUSER } from "../base/fetches/actionPrefixes";
import { SimpleLayout } from "../base/utils/SimpleLayout";
import { REDUX_SET_JWTTOKEN } from "../base/global/globalReducer";

const SetDetailItem = ({ title, value, style }) => (
  <View
    style={{
      backgroundColor: "white",
      borderBottomWidth: 1,
      borderTopWidth: 1,
      borderColor: "#aaa",
      padding: 10,
      width: "100%",
      flexDirection: "row",
      ...style
    }}
  >
    <View style={{ width: "30%" }}>
      <Text>{title}</Text>
    </View>
    <View style={{ width: "70%" }}>
      <Text>{value}</Text>
    </View>
  </View>
);

export const SetDetailView = HOCFetchEdit(
  connect(state => ({}))(
    injectIntl(
      class SetDetailView extends PureComponent {
        static propTypes = {
          handleViewChange: PropTypes.func.isRequired,
          intl: intlShape.isRequired
        };
        state = { content: {}, options: {} };

        async componentDidMount() {
          const { fetchDetail } = this.props;
          const { ok, content, options } = await fetchDetail(
            PREFIX_UAMUSER,
            "self"
          );
          if (ok) this.setState({ content, options });
        }

        handleLogout = () => {
          const { dispatch } = this.props;
          dispatch({ type: REDUX_SET_JWTTOKEN, payload: "" });
        };

        render() {
          const { content, options } = this.state;
          const { intl, handleViewChange } = this.props;

          if (!Object.keys(options).length) return <Loading />;

          return (
            <SimpleLayout
              title={intl.formatMessage({ id: "setDetailView.title" })}
            >
              <SetDetailItem
                title={options.username.label}
                value={content.username}
                style={{
                  paddingTop: 20
                }}
              />
              <SetDetailItem
                title={options.first_name.label}
                value={content.first_name}
              />
              <SetDetailItem
                title={options.last_name.label}
                value={content.last_name}
              />
              <SetDetailItem
                title={options.email.label}
                value={content.email}
              />
              <View style={{ marginTop: 30 }}>
                <Button
                  type="clear"
                  title={intl.formatMessage({ id: "common.update" })}
                  onPress={handleViewChange}
                />
              </View>
              <View style={{ marginTop: 30 }}>
                <Button
                  title={intl.formatMessage({ id: "setDetailView.logout" })}
                  onPress={this.handleLogout}
                />
              </View>
            </SimpleLayout>
          );
        }
      }
    )
  )
);
