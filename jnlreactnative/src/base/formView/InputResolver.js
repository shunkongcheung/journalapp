import React, { PureComponent } from "react";
import { injectIntl, intlShape } from "react-intl";
import { Text, View } from "react-native";
import PropTypes from "prop-types";

import { ButtonGroupField } from "./ButtonGroupField";
import { InputField } from "./InputField";
import { SelectField } from "./SelectField";
import { TextAreaField } from "./TextAreaField";

export const InputResolver = injectIntl(
  class InputResolver extends PureComponent {
    static propTypes = {
      error: PropTypes.string,
      intl: intlShape.isRequired,
      label: PropTypes.string.isRequired,
      read_only: PropTypes.bool,
      handleValueChange: PropTypes.func.isRequired
    };

    render() {
      const { intl, error, required, value, ...rest } = this.props;
      const isContainValue = value && value.length;
      return (
        <View
          style={{
            alignItems: "stretch",
            justifyContent: "center",
            paddingBottom: 13,
            width: "100%"
          }}
        >
          <InputSelector value={value} {...rest} />
          <Text
            style={{
              color: isContainValue && error ? "red" : "green",
              fontSize: 10,
              marginTop: 5
            }}
          >
            {isContainValue && error
              ? error
              : required
                ? intl.formatMessage({ id: "formView.inputResolver.required" })
                : null}
          </Text>
        </View>
      );
    }
  }
);
class InputSelector extends PureComponent {
  static propTypes = {
    type: PropTypes.oneOf([
      "button",
      "choice",
      "email",
      "float",
      "radio",
      "password",
      "string",
      "textarea"
    ]).isRequired
  };

  render() {
    const { type, ...rest } = this.props;
    switch (type) {
      case "button":
        return <ButtonGroupField {...rest} />;
      case "choice":
        return <SelectField {...rest} />;
      case "email":
        return <InputField keyboardType="email-address" {...rest} />;
      case "float":
      case "integer":
        return <InputField keyboardType="numeric" {...rest} />;
      case "radio":
        return <Text>radio</Text>;
      case "password":
        return (
          <InputField
            autoComplete="password"
            secureTextEntry={true}
            {...rest}
          />
        );
      case "string":
        return <InputField {...rest} />;
      case "textarea":
        return <TextAreaField {...rest} />;
    }
    return <Text>unrecognized type</Text>;
  }
}
