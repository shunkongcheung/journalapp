import React, { Fragment } from "react";
import { Button } from "react-native-elements";
import { injectIntl } from "react-intl";
import { InputResolver } from "./InputResolver";

const FormikOpionInputResolver = ({
  errors,
  optionKey,
  optionValue,
  setFieldValue,
  values
}) => {
  optionValue =
    typeof optionValue == "object" ? optionValue : optionValue(values);

  if (optionValue.read_only) return null;

  return (
    <InputResolver
      choices={optionValue.choices}
      error={errors[optionKey]}
      label={optionValue.label}
      required={optionValue.required}
      type={optionValue.type}
      value={values[optionKey]}
      handleValueChange={value => setFieldValue(optionKey, value)}
    />
  );
};

export const FormikView = injectIntl(
  ({
    errors,
    handleBack,
    handleSubmit,
    isSubmitting,
    isValid,
    intl,
    options,
    values,
    setFieldValue
  }) => (
    <Fragment>
      {Object.entries(options).map(([key, value]) => (
        <FormikOpionInputResolver
          errors={errors}
          key={key}
          optionKey={key}
          optionValue={value}
          setFieldValue={setFieldValue}
          values={values}
        />
      ))}
      <Button
        buttonStyle={{ width: "100%" }}
        disabled={!handleBack && (!isValid || isSubmitting)}
        loading={isSubmitting}
        onPress={!isValid && handleBack ? handleBack : handleSubmit}
        title={
          !isValid && handleBack
            ? intl.formatMessage({ id: "common.back" })
            : intl.formatMessage({ id: "common.submit" })
        }
      />
    </Fragment>
  )
);
