import React from "react";
import { Text, View } from "react-native";
import { ButtonGroup } from "react-native-elements";

export const ButtonGroupField = ({
  choices,
  disabled,
  label,
  handleValueChange,
  value,
  ...props
}) => (
  <View style={{ width: "100%" }}>
    <Text style={{ fontWeight: "bold" }}>{label}</Text>
    <ButtonGroup
      containerStyle={{ marginLeft: 0, marginRight: 0 }}
      disabled={disabled}
      onPress={index => handleValueChange(choices[index].value)}
      selectedIndex={choices.findIndex(item => item.value == value)}
      buttons={choices.map(item => item.display_name)}
    />
  </View>
);
