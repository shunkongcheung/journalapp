import React, { PureComponent } from "react";
import { Text, View } from "react-native";
import { injectIntl, intlShape } from "react-intl";
import PropTypes from "prop-types";
import MultiSelect from "react-native-multiple-select";

export const SelectField = injectIntl(
  class SelectField extends PureComponent {
    static propTypes = {
      choices: PropTypes.array.isRequired,
      intl: intlShape.isRequired,
      label: PropTypes.string.isRequired,
      multiple: PropTypes.bool,
      handleValueChange: PropTypes.func.isRequired,
      value: PropTypes.array
    };

    multiSelectRef = null;
    render() {
      const {
        choices,
        label,
        intl,
        multiple,
        handleValueChange,
        value,
        ...rest
      } = this.props;
      return (
        <View style={{ width: "100%", justifyContent: "center" }}>
          <Text style={{ fontSize: 20, fontWeight: "bold", marginBottom: 10 }}>
            {label}
          </Text>
          <MultiSelect
            hideTags
            items={choices.map(choice => ({
              ...choice,
              value: choice.value.toString()
            }))}
            uniqueKey="value"
            ref={component => (this.multiSelect = component)}
            onSelectedItemsChange={handleValueChange}
            selectedItems={value}
            selectText={intl.formatMessage({
              id: "formView.selectField.searchPlaceholder"
            })}
            searchInputPlaceholderText={intl.formatMessage({
              id: "formView.selectField.searchPlaceholder"
            })}
            single={!multiple}
            tagRemoveIconColor="#CCC"
            tagBorderColor="#CCC"
            tagTextColor="#CCC"
            selectedItemTextColor="#CCC"
            selectedItemIconColor="#CCC"
            itemTextColor="#000"
            displayKey="display_name"
            searchInputStyle={{
              color: "#CCC",
              fontWeight: "bold",
              lineHeight: 30,
              padding: 10
            }}
            submitButtonColor="#CCC"
            submitButtonText="Submit"
            {...rest}
          />
          <View>
            {this.multiSelect && multiple
              ? this.multiSelect.getSelectedItemsExt(value)
              : null}
          </View>
        </View>
      );
    }
  }
);
