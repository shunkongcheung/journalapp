import React from "react";
import { StyleSheet, TextInput, Text, View } from "react-native";

export const TextAreaField = ({ handleValueChange, label, value, ...rest }) => (
  <View style={styles.textAreaContainer}>
    <TextInput
      placeholder={label}
      numberOfLines={10}
      multiline={true}
      onChangeText={handleValueChange}
      style={styles.textArea}
      value={value}
      {...rest}
    />
  </View>
);
const styles = StyleSheet.create({
  textAreaContainer: {
    marginTop: 5,
    width: "100%"
  },
  textArea: {
    borderColor: "grey",
    borderRadius: 5,
    borderWidth: 1,
    height: 80,
    width: "100%",
    padding: 10,
    justifyContent: "flex-start"
  }
});
