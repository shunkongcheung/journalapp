import React, { PureComponent } from "react";
import { Animated, Platform, TextInput, View } from "react-native";
import { Icon } from "react-native-elements";

export class InputField extends PureComponent {
  state = { isFocused: false };

  _animatedIsFocused = new Animated.Value(this.props.value === "" ? 0 : 1);

  handleFocus = () => {
    const { onFocus } = this.props;
    this.setState({ isFocused: true });
    if (onFocus) onFocus();
  };
  handleBlur = () => {
    const { onBlur } = this.props;
    this.setState({ isFocused: false });
    if (onBlur) onBlur();
  };

  componentDidUpdate() {
    Animated.timing(this._animatedIsFocused, {
      toValue: this.state.isFocused || this.props.value ? 1 : 0,
      duration: 200
    }).start();
  }

  render() {
    const { handleValueChange, label, read_only, ...rest } = this.props;

    const labelStyle = {
      position: "absolute",
      left: 5,
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [18, 0]
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [17, 12]
      }),
      color: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: ["#aaa", "#000"]
      })
    };
    return (
      <View style={{ width: "100%", paddingTop: 18 }}>
        <Animated.Text style={labelStyle}>{label}</Animated.Text>
        <TextInput
          {...rest}
          style={{
            borderBottomWidth: 1,
            borderBottomColor: "#aaa",
            color: "#000",
            fontSize: 15,
            ...Platform.select({
              ios: {
                height: 26
              },
              android: {
                height: 40
              }
            }),
            paddingLeft: 10
          }}
          onChangeText={handleValueChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          blurOnSubmit
        />
      </View>
    );
  }
}
