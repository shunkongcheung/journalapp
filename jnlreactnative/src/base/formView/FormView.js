import React, { PureComponent } from "react";
import { injectIntl } from "react-intl";
import { Alert, Text, View } from "react-native";
import { Formik } from "formik";
import * as yup from "yup";
import PropTypes from "prop-types";
import { FormikView } from "./FormikView";

import { Loading } from "../utils/Loading";
import { SimpleLayout } from "../utils/SimpleLayout";

export const FormView = injectIntl(
  class FormView extends PureComponent {
    static propTypes = {
      handleBack: PropTypes.func,
      handleSubmit: PropTypes.func.isRequired,
      initialValues: PropTypes.object,
      options: PropTypes.object.isRequired,
      title: PropTypes.string.isRequired,
      validationSchemaShape: PropTypes.object
    };

    state = { validationSchemaShape: {} };

    componentDidMount() {
      this.getValidationSchemaShape();
    }

    componentDidUpdate() {
      this.getValidationSchemaShape();
    }

    getYupType = type => {
      switch (type) {
        case "choice":
          return yup.array();
        case "email":
          return yup.string().email();
        case "float":
          return yup.number();
        case "string":
          return yup.string();
        default:
          return yup.mixed();
      }
    };

    getValidationSchemaShape = () => {
      let { options, validationSchemaShape } = this.props;

      // no options. cant set
      if (!Object.keys(options).length) return;
      // already set. skip
      if (Object.keys(this.state.validationSchemaShape).length) return;

      validationSchemaShape = validationSchemaShape || {};

      Object.entries(options).map(([key, value]) => {
        value = typeof value == "object" ? value : { type: "undefined" };

        // if schema already exist. skip
        if (!validationSchemaShape[key]) {
          const { type } = value;
          validationSchemaShape[key] = this.getYupType(value.type);
        }
        // mark required
        if (value.required && !value.read_only) {
          validationSchemaShape[key] = validationSchemaShape[key].required();
        }
        // add choices
        // if (value.choices) {
        //   validationSchemaShape[key] = validationSchemaShape[key].oneOf(
        //     value.choices.map(item => item.value)
        //   );
        // }
      });
      this.setState({ validationSchemaShape });
    };

    render() {
      const {
        children,
        handleBack,
        handleSubmit,
        initialValues,
        intl,
        options,
        title
      } = this.props;
      const { validationSchemaShape } = this.state;

      if (!Object.keys(validationSchemaShape).length) return <Loading />;

      return (
        <SimpleLayout title={title}>
          <Formik
            initialValues={initialValues}
            validationSchema={yup.object().shape(validationSchemaShape)}
            onSubmit={(data, apis) => {
              apis.setSubmitting(true);
              handleSubmit({ ...data }, apis);
              apis.setSubmitting();
            }}
          >
            {props => {
              Object.entries(props.errors).map(([key, value]) => {
                if (!options[key]) Alert.alert(JSON.stringify(value));
              });
              return (
                <FormikView
                  {...props}
                  handleBack={handleBack}
                  options={options}
                />
              );
            }}
          </Formik>
          {children}
        </SimpleLayout>
      );
    }
  }
);
