export default {
  "bgtItem.amount": "You have spent {amount} dollar.",

  "bgtFormView.title": "Add an expenditure",
  "bgtFormView.locationNameLabel": "location name",
  "bgtFormView.catagoryNameLabel": "catagory name",
  "bgtFormView.chooseCat": "Choose catagory",
  "bgtFormView.createCat": "Create catagory",

  "bgtMapView.search": "search",
  "bgtMapView.search": "search",
  "bgtMapView.locMaster.desc":
    "there were {times} times of spending in this place",
  "bgtMapView.custLocDesc": "customer location",
  "bgtMapView.custLocName": "new location",

  "common.back": "Back",
  "common.submit": "Submit",
  "common.update": "Update",

  "forgetPwdView.title": "Forget password",

  "formView.inputResolver.required": "Required *",
  "formView.selectField.searchPlaceholder": "Search...",

  "hocFetch.failContent":
    "You do not have access to the internet.\nThe application may not function properly",
  "hocFetch.failTitle": "Connection Error",

  "hmeContainer.welcome": "Welcome back",
  "hmeContainer.filter.weekly": "weekly",
  "hmeContainer.filter.monthly": "monthly",
  "hmeContainer.filter.yearly": "yearly",
  "hmeBarChartView.info": "{title}: {value}/{total} ({percent}%)",
  "hmeContainer.chartTitle.catagory": "Expenditure per catagories",
  "hmeContainer.chartTitle.paymentMethod": "Expenditure per payment method",

  "loginView.title": "Login",
  "loginView.forgetPwd": "forget password",
  "loginView.register": "register",
  "loginView.username": "username",
  "loginView.password": "password",

  "registerView.title": "Register",

  "setDetailView.title": "Setting",
  "setDetailView.logout": "Logout",

  "setUpdateView.confirm_password": "confirm password",
  "setUpdateView.title": "Update Setting"
};
