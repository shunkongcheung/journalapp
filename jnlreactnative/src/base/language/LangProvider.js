import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { IntlProvider } from "react-intl";

import langEn from "./en.js";

export const LangProvider = connect(state => ({
  language: state.globalReducer.language
}))(
  class LangProvider extends PureComponent {
    render() {
      const { children, language } = this.props;
      const messages = { en: langEn };

      return (
        <IntlProvider locale="en" messages={messages[language]}>
          {children}
        </IntlProvider>
      );
    }
  }
);
