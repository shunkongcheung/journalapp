import React, { PureComponent } from "react";
import { Alert, AsyncStorage, Text } from "react-native";
import { connect, Provider } from "react-redux";
import { createStore } from "redux";

import globalReducer, {
  REDUX_SET_JWTTOKEN,
  REDUX_SET_USERCOORD
} from "./globalReducer";
import { HOCFetch } from "../fetches/HOCFetch";
import { LoginContainer } from "./LoginContainer";

const JWTTOKEN_KEY = "whkasdf";

export const GlobalConfigurator = connect(state => ({
  jwtToken: state.globalReducer.jwtToken
}))(
  HOCFetch(
    class InnerReduxConfigurator extends PureComponent {
      async componentDidMount() {
        const { dispatch, makeFetch } = this.props;
        navigator.geolocation.getCurrentPosition(position => {
          dispatch({ type: REDUX_SET_USERCOORD, payload: position.coords });
        });

        try {
          const jwtToken = await AsyncStorage.getItem(JWTTOKEN_KEY);
          const { ok, payload } = await makeFetch("token/refresh/", "POST", {
            token: jwtToken
          });
          if (ok) {
            const { token } = payload;
            dispatch({ type: REDUX_SET_JWTTOKEN, payload: token });
            await AsyncStorage.setItem(JWTTOKEN_KEY, token);
          }
        } catch (error) {
          dispatch({ type: REDUX_SET_JWTTOKEN, payload: "" });
          console.log(
            `GlobalConfigurator: restrieving jwt / refresh jwt - ${JSON.stringify(
              error
            )}`
          );
        }
      }

      async componentDidUpdate() {
        const { jwtToken } = this.props;
        if (!jwtToken) await AsyncStorage.setItem(JWTTOKEN_KEY, "");
      }

      handleJwt = async jwtToken => {
        const { dispatch } = this.props;
        dispatch({ type: REDUX_SET_JWTTOKEN, payload: jwtToken });
        await AsyncStorage.setItem(JWTTOKEN_KEY, jwtToken);
      };

      render() {
        const { jwtToken } = this.props;
        // return this.props.children;
        if (jwtToken) return this.props.children;
        else return <LoginContainer handleJwt={this.handleJwt} />;
      }
    }
  )
);

export const store = createStore(globalReducer);
