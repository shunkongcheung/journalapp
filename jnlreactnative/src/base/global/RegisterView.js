import React, { Fragment, PureComponent } from "react";
import { injectIntl } from "react-intl";
import PropTypes from "prop-types";

import { FormView } from "../formView/FormView";
import { HOCFetchEdit } from "../fetches/HOCFetchEdit";

export const RegisterView = injectIntl(
  HOCFetchEdit(
    class RegisterView extends PureComponent {
      static propTypes = {
        handleViewChange: PropTypes.func.isRequired
      };
      state = { options: {} };

      async componentDidMount() {
        const { fetchOptions } = this.props;
        const { options, ok } = await fetchOptions("user_account/uam_user");
        this.setState({ options });
      }

      handleSubmit = async (data, apis) => {
        const { handleViewChange, fetchSubmitCreate } = this.props;
        const { errors, ok } = await fetchSubmitCreate(
          "user_account/uam_user",
          data
        );

        if (ok) handleViewChange("loginView");
        else apis.setErrors(errors);
      };

      render() {
        const { options } = this.state;
        const { handleViewChange, intl } = this.props;
        return (
          <FormView
            title={intl.formatMessage({ id: "registerView.title" })}
            handleBack={() => handleViewChange("loginView")}
            handleSubmit={this.handleSubmit}
            options={options}
          />
        );
      }
    }
  )
);
