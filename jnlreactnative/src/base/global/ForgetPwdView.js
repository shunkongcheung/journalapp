import React, { Fragment, PureComponent } from "react";
import { injectIntl } from "react-intl";
import PropTypes from "prop-types";

import { FormView } from "../formView/FormView";
import { HOCFetch } from "../fetches/HOCFetch";

export const ForgetPwdView = injectIntl(
  HOCFetch(
    class ForgetPwdView extends PureComponent {
      static propTypes = {
        handleViewChange: PropTypes.func.isRequired
      };
      state = { options: {} };

      async componentDidMount() {
        const { makeFetch } = this.props;
        const { payload, ok } = await makeFetch(
          "user_account/uam_user/pwd_reset/",
          "OPTIONS"
        );
        const options = payload.actions.POST;
        options.email.type = "email";
        this.setState({ options });
      }

      handleSubmit = async (data, apis) => {
        const { handleViewChange, makeFetch } = this.props;
        const { payload, ok } = await makeFetch(
          "user_account/uam_user/pwd_reset/",
          "POST",
          data
        );

        console.log("hey here...", ok, payload);
        if (ok) handleViewChange("loginView");
        else apis.setErrors(payload);
      };

      render() {
        const { options } = this.state;
        const { handleViewChange, intl } = this.props;
        return (
          <FormView
            title={intl.formatMessage({ id: "forgetPwdView.title" })}
            handleBack={() => handleViewChange("loginView")}
            handleSubmit={this.handleSubmit}
            options={options}
          />
        );
      }
    }
  )
);
