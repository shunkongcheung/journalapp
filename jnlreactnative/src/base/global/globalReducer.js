import { combineReducers } from "redux";

export const REDUX_SET_LANGUAGE = "REDUX_SET_LANGUAGE";
export const REDUX_SET_JWTTOKEN = "REDUX_SET_JWTTOKEN";
export const REDUX_SET_USERCOORD = "REDUX_SET_USERCOORD";

const INITIAL_STATE = {
  language: "en",
  jwtToken: "",
  userCoord: {}
};

const globalReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REDUX_SET_LANGUAGE:
      return { ...state, language: action.payload };
    case REDUX_SET_JWTTOKEN:
      return { ...state, jwtToken: action.payload };
    case REDUX_SET_USERCOORD:
      return { ...state, userCoord: action.payload };
    default:
      return state;
  }
};

export default combineReducers({ globalReducer });
