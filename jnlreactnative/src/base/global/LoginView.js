import React, { Fragment, PureComponent } from "react";
import { View } from "react-native";
import { Button } from "react-native-elements";
import { injectIntl, intlShape } from "react-intl";
import PropTypes from "prop-types";

import { FormView } from "../formView/FormView";
import { HOCFetch } from "../fetches/HOCFetch";

export const LoginView = injectIntl(
  HOCFetch(
    class LoginView extends PureComponent {
      static propTypes = {
        handleJwt: PropTypes.func.isRequired,
        handleViewChange: PropTypes.func.isRequired
      };

      handleSubmit = async (data, apis) => {
        const { handleJwt, makeFetch } = this.props;
        const { payload, ok } = await makeFetch("token/obtain/", "POST", data);

        if (ok) handleJwt(payload.token);
        else apis.setErrors(content);
      };

      render() {
        const { intl, handleViewChange } = this.props;
        return (
          <FormView
            title={intl.formatMessage({ id: "loginView.title" })}
            handleSubmit={this.handleSubmit}
            options={{
              username: {
                label: intl.formatMessage({ id: "loginView.username" }),
                type: "string",
                required: true
              },
              password: {
                label: intl.formatMessage({ id: "loginView.password" }),
                type: "password",
                required: true
              }
            }}
          >
            <View style={{ marginTop: 50 }}>
              <Button
                onPress={() => handleViewChange("forgetPwdView")}
                type="clear"
                title={intl.formatMessage({
                  id: "loginView.forgetPwd"
                })}
              />
              <Button
                type="clear"
                onPress={() => handleViewChange("registerView")}
                title={intl.formatMessage({
                  id: "loginView.register"
                })}
              />
            </View>
          </FormView>
        );
      }
    }
  )
);
