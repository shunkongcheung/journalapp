import React, { Fragment, PureComponent } from "react";
import { View } from "react-native";
import { Button } from "react-native-elements";
import { injectIntl, intlShape } from "react-intl";
import PropTypes from "prop-types";

import { FormView } from "../formView/FormView";
import { HOCFetch } from "../fetches/HOCFetch";
import { DEFAULT_DURATION, FadeView } from "../utils/FadeView";
import { LoginView } from "./LoginView";
import { ForgetPwdView } from "./ForgetPwdView";
import { RegisterView } from "./RegisterView";

export class LoginContainer extends PureComponent {
  static propTypes = {
    handleJwt: PropTypes.func.isRequired
  };

  state = { viewName: "loginView" };

  handleViewChange = viewName => {
    this.setState({ viewName: "" });
    setTimeout(() => this.setState({ viewName }), DEFAULT_DURATION);
  };

  render() {
    const { handleJwt } = this.props;
    const { viewName } = this.state;

    return (
      <View style={{ backgroundColor: "white", flex: 1 }}>
        <FadeView
          isVisible={viewName == "loginView"}
          style={{ height: "100%" }}
        >
          <LoginView
            handleJwt={handleJwt}
            handleViewChange={this.handleViewChange}
          />
        </FadeView>
        <FadeView
          isVisible={viewName == "forgetPwdView"}
          style={{ height: "100%" }}
        >
          <ForgetPwdView handleViewChange={this.handleViewChange} />
        </FadeView>
        <FadeView
          isVisible={viewName == "registerView"}
          style={{ height: "100%" }}
        >
          <RegisterView handleViewChange={this.handleViewChange} />
        </FadeView>
      </View>
    );
  }
}
