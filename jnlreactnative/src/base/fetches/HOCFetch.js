import React, { Fragment, PureComponent } from "react";
import { NetInfo, Text, View } from "react-native";
import { injectIntl } from "react-intl";
import { connect } from "react-redux";
import { MyModal } from "../utils/MyModal";

export const HOCFetch = WrappedComponent =>
  connect(state => ({
    language: state.globalReducer.language,
    jwtToken: state.globalReducer.jwtToken
  }))(
    injectIntl(
      class HOCFetch extends PureComponent {
        state = { isConnected: true };

        getHeaders = () => {
          const { language, jwtToken } = this.props;
          return {
            "content-type": "application/json",
            Authorization: jwtToken ? "JWT " + jwtToken : "",
            "Accept-Language": language
          };
        };

        makeFetch = async (url, method = "GET", data = {}) => {
          method = method.toUpperCase();
          const headers = this.getHeaders();

          const isBodyUseful = method == "PUT" || method == "POST";
          const body = isBodyUseful ? JSON.stringify(data) : null;

          const isConnected = await NetInfo.isConnected.fetch();
          this.setState({ isConnected });

          const response = await fetch(`https://journalapp.tk/api/${url}`, {
            headers,
            body,
            method
          });

          const isParseJson = response.ok && method != "DELETE";
          let payload = isParseJson
            ? await response.json()
            : await response.text();
          try {
            payload = JSON.parse(payload);
          } catch (ex) {}

          return { ok: response.ok, payload };
        };

        render() {
          const { isConnected } = this.state;
          const { intl } = this.props;
          return (
            <Fragment>
              <WrappedComponent
                handleFetchFailure={this.handleFetchFailure}
                makeFetch={this.makeFetch}
                {...this.props}
              />
              <MyModal
                isVisible={!isConnected}
                title={intl.formatMessage({ id: "hocFetch.failTitle" })}
                handleModalClose={() => this.setState({ isConnected: true })}
              >
                <Text>
                  {intl.formatMessage({ id: "hocFetch.failContent" })}
                </Text>
              </MyModal>
            </Fragment>
          );
        }
      }
    )
  );
