import React, { PureComponent } from "react";
import { HOCFetch } from "./HOCFetch";
// import { HOCFetchDetail } from "./HOCFetchDetail";
import PropTypes from "prop-types";

export const HOCFetchEdit = WrappedComponent =>
  HOCFetch(
    class HOCFetchEdit extends PureComponent {
      static propTypes = { makeFetch: PropTypes.func.isRequired };

      fetchDelete = async (prefix, id) => {
        const { makeFetch } = this.props;
        const response = await makeFetch(`/api/${prefix}/${id}/`, "DELETE");
        const { ok, payload } = response;

        if (ok) return { content: payload, ok };
        else return { errors: payload, ok };
      };

      fetchDetail = async (prefix, id) => {
        const { makeFetch } = this.props;
        const res = await Promise.all([
          makeFetch(`/api/${prefix}/${id}/`, "GET"),
          this.fetchOptions(prefix, id)
        ]);
        const { ok: dOk, payload: content } = res[0];
        const { ok: oOk, options } = res[1];

        if (!dOk) return { errors: content, ok: false };
        if (!oOk) return { errors: options, ok: false };
        return { content, options, ok: true };
      };

      fetchOptions = async (prefix, id) => {
        const { makeFetch } = this.props;
        const response = await makeFetch(
          `/api/${prefix}/${id ? id : "create"}/`,
          "OPTIONS"
        );
        const { ok, payload } = response;
        if (ok)
          return { ok, options: id ? payload.mapping : payload.actions.POST };
        else return { ok, errors: payload };
      };

      fetchSubmitCreate = async (prefix, data) => {
        const { makeFetch } = this.props;
        const response = await makeFetch(
          `/api/${prefix}/create/`,
          "POST",
          data
        );
        const { ok, payload } = response;

        if (ok) return { content: payload, ok };
        else return { errors: payload, ok };
      };

      fetchSubmitUpdate = async (prefix, id, data) => {
        const { makeFetch } = this.props;
        const response = await makeFetch(`/api/${prefix}/${id}/`, "PUT", data);
        const { ok, payload } = response;
        if (ok) return { content: payload, ok };
        else return { errors: payload, ok };
      };

      render() {
        const rest = { ...this.props };
        delete rest.makeFetch;
        return (
          <WrappedComponent
            fetchDelete={this.fetchDelete}
            fetchDetail={this.fetchDetail}
            fetchOptions={this.fetchOptions}
            fetchSubmitCreate={this.fetchSubmitCreate}
            fetchSubmitUpdate={this.fetchSubmitUpdate}
            {...rest}
          />
        );
      }
    }
  );
