import React, { PureComponent } from "react";
import { HOCFetch } from "./HOCFetch";
import PropTypes from "prop-types";

export const HOCFetchList = WrappedComponent =>
  HOCFetch(
    class HOCFetchList extends PureComponent {
      static propTypes = { makeFetch: PropTypes.func.isRequired };

      fetchList = async (
        prefix,
        page = 1,
        filterBy = "",
        orderBy = "",
        otherQuerys = {}
      ) => {
        const { makeFetch } = this.props;

        const otherQueryString = otherQuerys
          ? Object.keys(otherQuerys)
              .map(key => "&" + key + "=" + otherQuerys[key])
              .join("")
          : "";

        const url =
          `/api/${prefix}/list/?page=${page}&` +
          `filter_by=${filterBy}&order_by=${orderBy}` +
          otherQueryString;

        const responses = await Promise.all([
          makeFetch(url, "OPTIONS"),
          makeFetch(url)
        ]);

        if (!responses[0].ok)
          return { errors: responses[0].payload, ok: false };

        if (!responses[1].ok)
          return { errors: responses[1].payload, ok: false };

        return {
          content: responses[1].payload.results,
          ok: true,
          options: responses[0].payload.mapping
        };
      };

      render() {
        const rest = { ...this.props };
        delete rest.makeFetch;
        return <WrappedComponent fetchList={this.fetchList} {...rest} />;
      }
    }
  );
