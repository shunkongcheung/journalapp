export const PREFIX_BGTCATAGORY = "budget/bgt_catagory";
export const PREFIX_BGTMASTER = "budget/bgt_master";

export const PREFIX_LOCMASTER = "location/loc_master";
export const PREFIX_LOCSEARCH = "location/loc_search";

export const PREFIX_UAMUSER = "user_account/uam_user";
