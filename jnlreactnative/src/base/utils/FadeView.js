import React, { PureComponent } from "react";
import { Animated, Text, View } from "react-native";
import PropTypes from "prop-types";

export const DEFAULT_DURATION = 500;

export class FadeView extends PureComponent {
  static propTypes = {
    duration: PropTypes.number,
    isVisible: PropTypes.bool,
    isFromTop: PropTypes.bool,
    isOccupyByDefault: PropTypes.bool,
    isOccupyOnInvisible: PropTypes.bool
  };

  bottom = new Animated.Value(20);
  opacity = new Animated.Value(0);

  state = { isVisible: this.props.isVisible };

  componentDidMount = () => this.triggerAnimation(true);
  componentDidUpdate = () => this.triggerAnimation();

  triggerAnimation = isMount => {
    const { duration, isVisible } = this.props;

    if (!isMount && this.state.isVisible == isVisible) return;
    if (isVisible) this.setState({ isVisible });

    Animated.parallel([
      Animated.timing(this.opacity, {
        toValue: isVisible ? 1 : 0,
        duration: duration || DEFAULT_DURATION
      }),
      Animated.timing(this.bottom, {
        toValue: isVisible ? 0 : 20,
        duration: duration || DEFAULT_DURATION
      })
    ]).start(() => this.setState({ isVisible }));
  };

  render() {
    const { isFromTop, isOccupyOnInvisible } = this.props;
    const { isVisible } = this.state;
    const bottom = isVisible && isFromTop ? this.bottom : 0;
    const opacity = isVisible ? this.opacity : 0;

    if (isVisible)
      return (
        <Animated.View
          style={{
            bottom,
            opacity,
            position: "relative",
            ...this.props.style
          }}
        >
          {this.props.children}
        </Animated.View>
      );

    // this is to avoid component from moving
    if (isOccupyOnInvisible)
      return <View style={{ opacity: 0 }}>{this.props.children}</View>;
    return null;
  }
}
