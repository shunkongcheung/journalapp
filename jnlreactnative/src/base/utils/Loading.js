import React from "react";
import { Text, View } from "react-native";

export const Loading = ({}) => (
  <View style={{ flex: 1, alignItems: "center" }}>
    <Text>loading...</Text>
  </View>
);
