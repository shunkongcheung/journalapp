import React from "react";
import { Text, View } from "react-native";

export const Title = ({ height = 50, title }) => (
  <View
    style={{
      alignItems: "center",
      borderBottomWidth: 1,
      borderColor: "#aaa",
      height,
      justifyContent: "center",
      width: "100%",

      // Android
      elevation: 2,

      // iOS
      shadowColor: "#000000",
      shadowOpacity: 0.4,
      shadowRadius: 1,
      shadowOffset: {
        height: 1,
        width: 0
      }
    }}
  >
    <Text
      style={{
        fontWeight: "bold",
        fontSize: 20
      }}
    >
      {title}
    </Text>
  </View>
);
