import React from "react";
import { ScrollView, Text, TouchableWithoutFeedback, View } from "react-native";
import { Icon } from "react-native-elements";
import Modal from "react-native-modal";

export const MyModal = ({ children, handleModalClose, isVisible, title }) => (
  <Modal isVisible={isVisible}>
    <ScrollView
      style={{
        backgroundColor: "white",
        borderRadius: 10,
        height: "100%",
        padding: 10
      }}
    >
      <View style={{ alignItems: "flex-end" }}>
        <TouchableWithoutFeedback onPress={handleModalClose}>
          <Icon name="clear" />
        </TouchableWithoutFeedback>
      </View>
      <View
        style={{
          alignItems: "center",
          width: "100%"
        }}
      >
        <Text
          style={{
            fontWeight: "bold",
            fontSize: 25,
            marginBottom: 10
          }}
        >
          {title}
        </Text>
      </View>

      <View
        style={{
          alignItems: "flex-start",
          width: "100%"
        }}
      >
        {children}
      </View>
    </ScrollView>
  </Modal>
);
