import React, { PureComponent } from "react";
import { injectIntl, intlShape } from "react-intl";
import { Text, View } from "react-native";
import { Calendar, CalendarList, Agenda } from "react-native-calendars";
import PropTypes from "prop-types";

import { HOCFetchList } from "../fetches/HOCFetchList";
import {
  PREFIX_BGTCATAGORY,
  PREFIX_BGTMASTER,
  PREFIX_LOCMASTER
} from "../fetches/actionPrefixes";

export const CalendarView = HOCFetchList(
  injectIntl(
    class CalendarView extends PureComponent {
      static propTypes = {
        fetchPrefix: PropTypes.string.isRequired,
        fetchItemsForMonth: PropTypes.func,
        intl: intlShape.isRequired,
        renderItem: PropTypes.func.isRequired
      };
      state = {
        data: [],
        selectedDate: { dateString: new Date().toISOString().slice(0, 10) },
        refreshing: false
      };

      async componentDidMount() {
        const td = new Date();
        const year = td.getFullYear();
        const month = td.getMonth();
        this.fetchItemsForMonth({ year, month });
      }

      fetchItemsForMonth = async ({ year, month }) => {
        const { fetchItemsForMonth, fetchList, fetchPrefix } = this.props;

        const fd = new Date(year, month, 1);
        const ld = new Date(year, month + 1, 0);
        const n = num => (num > 9 ? num : `0${num}`);
        const q = {
          page_size: 10000,
          created_at__lte: `${ld.getFullYear()}-${n(ld.getMonth() + 1)}-${n(
            ld.getDate()
          )}`,
          created_at__gte: `${fd.getFullYear()}-${n(fd.getMonth() + 1)}-${n(
            fd.getDate()
          )}`
        };

        this.setState({ refreshig: true });
        const { content, ok } = await fetchList(fetchPrefix, 1, "", "", q);
        if (ok) {
          this.setState({ data: content });
          if (fetchItemsForMonth) await fetchItemsForMonth(content);
        }

        this.setState({ refreshig: false });
      };

      getDateFromCreatedAt = createdAt =>
        new Date(createdAt).toISOString().slice(0, 10);

      getItems = () => {
        const { data, selectedDate } = this.state;
        const items = {};
        data.map(item => {
          const itemDate = this.getDateFromCreatedAt(item.created_at);
          if (Array.isArray(items[itemDate])) {
            items[itemDate].push(item);
          } else {
            items[itemDate] = [item];
          }
        });
        if (!Array.isArray(items[selectedDate.dateString]))
          items[selectedDate.dateString] = [];
        return items;
      };

      getMarkedDates = () => {
        const { data, selectedDate } = this.state;
        const colors = ["red", "pink", "purple", "green"];
        const markedDates = {};

        data.map(item => {
          const itemDate = this.getDateFromCreatedAt(item.created_at);
          if (typeof markedDates[itemDate] == "object") {
            markedDates[itemDate].dots.push({
              ...item,
              color: colors[markedDates[itemDate].dots.length % colors.length]
            });
          } else {
            markedDates[itemDate] = { dots: [{ ...item, color: colors[0] }] };
          }
        });
        return markedDates;
      };

      handleSelectedDateChange = selectedDate => {
        this.setState({ selectedDate });
      };

      render() {
        const { refreshing } = this.state;
        const { intl, renderItem } = this.props;

        const items = this.getItems();
        const markedDates = this.getMarkedDates();

        return (
          <View
            style={{
              //	marginTop: 100,
              height: "100%"
            }}
          >
            <Agenda
              // the list of items that have to be displayed in agenda.
              items={items}
              // callback that gets called when items for a certain month should be loaded (month became visible)
              loadItemsForMonth={this.fetchItemsForMonth}
              // callback that gets called on day press
              onDayPress={this.handleSelectedDateChange}
              // Max amount of months allowed to scroll to the past. Default = 50
              pastScrollRange={50}
              // Max amount of months allowed to scroll to the future. Default = 50
              futureScrollRange={50}
              // specify what should be rendered instead of ActivityIndicator
              renderEmptyDate={() => <View />}
              // specify how each item should be rendered in agenda
              renderItem={(item, firstItemInDay) => renderItem({ item })}
              rowHasChanged={(r1, r2) => r1.id !== r2.id}
              // By default, agenda dates are marked if they have at least one item, but you can override this if needed
              markedDates={markedDates}
              markingType={"multi-dot"}
              // Set this true while waiting for new data from a refresh
              refreshing={refreshing}
              // agenda theme
              theme={{
                agendaDayTextColor: "yellow",
                agendaDayNumColor: "green",
                agendaTodayColor: "red",
                agendaKnobColor: "blue"
              }}
            />
          </View>
        );
      }
    }
  )
);
