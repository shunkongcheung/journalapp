import React, { Fragment, PureComponent } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { View } from "react-native";
import { SearchBar } from "react-native-elements";
import PropTypes from "prop-types";
import MapBaseView, { Marker } from "react-native-maps";

import { HOCFetchList } from "../fetches/HOCFetchList";
import { MyModal } from "../utils/MyModal";
import { PREFIX_LOCMASTER, PREFIX_LOCSEARCH } from "../fetches/actionPrefixes";

export const MapView = HOCFetchList(
  connect(state => ({ userCoord: state.globalReducer.userCoord }))(
    injectIntl(
      class MapView extends PureComponent {
        static propTypes = {
          fetchFromLocations: PropTypes.func,
          getLocationDesc: PropTypes.func.isRequired,
          handleMarkerPress: PropTypes.func.isRequired,
          userLocationQuery: PropTypes.object.isRequired,
          userCoord: PropTypes.object.isRequired,
          renderLocation: PropTypes.func.isRequired
        };
        state = {
          customLocations: [],
          detailLocation: {},
          lastFetchUserCoord: {
            latitude: -10000,
            longitude: -100000,
            latitudeDelta: 0.0,
            longitudeDelta: 0.0
          },
          locations: [],
          mapRegion: {
            ...this.props.userCoord,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
          },
          searchWord: "",
          searchLoading: false
        };

        _isMount = false;

        markerRefs = [];

        componentDidMount() {
          this._isMount = true;
        }

        componentWillUnmount() {
          this._isMount = false;
        }

        fetchUserLocations = async () => {
          if (!this._isMount) return;

          const {
            fetchFromLocations,
            fetchList,
            getLocationDesc,
            userLocationQuery
          } = this.props;
          const { lastFetchUserCoord, mapRegion, searchLoading } = this.state;
          if (searchLoading) return;

          const lastLat = lastFetchUserCoord.latitude;
          const lastLon = lastFetchUserCoord.longitude;
          const lastLatdelta = lastFetchUserCoord.latitudeDelta;
          const lastLondelta = lastFetchUserCoord.longitudeDelta;

          const curLat = mapRegion.latitude;
          const curLon = mapRegion.longitude;
          const curLatdelta = mapRegion.latitudeDelta;
          const curLondelta = mapRegion.longitudeDelta;

          const isWithinLastCoord =
            lastLat + lastLatdelta > curLat + curLatdelta &&
            lastLat - lastLatdelta < curLat - curLatdelta &&
            lastLon + lastLondelta > curLon + curLondelta &&
            lastLon - lastLondelta < curLon - curLondelta;

          if (isWithinLastCoord) return;

          const nextCoord = { ...mapRegion };
          nextCoord.latitudeDelta *= 4.0;
          nextCoord.longitudeDelta *= 4.0;

          this.setState({ searchLoading: true, lastFetchUserCoord: nextCoord });
          const { content, ok } = await fetchList(PREFIX_LOCMASTER, 1, "", "", {
            page_size: 10000,
            latitude__lte: nextCoord.latitude + nextCoord.latitudeDelta,
            latitude__gte: nextCoord.latitude - nextCoord.latitudeDelta,
            longitude__lte: nextCoord.longitude + nextCoord.longitudeDelta,
            longitude__gte: nextCoord.longitude - nextCoord.longitudeDelta,
            ...userLocationQuery
          });

          if (ok) {
            this.setState({ locations: content });
            if (fetchFromLocations) await fetchFromLocations(content);

            const locations = content.map(
              location => (location.description = getLocationDesc(location))
            );
          }
          this.setState({ searchLoading: false });
        };

        handleMapLongPress = ({ nativeEvent }) => {
          const { customLocations: oldLocations } = this.state;
          const { intl } = this.props;

          if (!this._isMount) return;
          const customLocations = [...oldLocations];
          const { coordinate } = nativeEvent;

          const description = intl.formatMessage({
            id: "bgtMapView.custLocDesc"
          });

          const name = intl.formatMessage({
            id: "bgtMapView.custLocName"
          });
          customLocations.push({ ...coordinate, description, name });
          this.setState({ customLocations });
        };

        handleMapRegionChange = async mapRegion => {
          if (!this._isMount) return;
          this.setState({ mapRegion });
          if (!this.state.searchWord) await this.fetchUserLocations();
        };

        handleSearchChange = async searchWord => {
          if (!this._isMount) return;
          if (!searchWord) await this.fetchUserLocations();
          this.setState({ searchWord });
        };

        handleSearchSubmit = async () => {
          // longitude: -122.406417, latitude: 37.78583
          const { fetchList } = this.props;
          const { searchLoading, searchWord, mapRegion } = this.state;
          if (searchLoading || !searchWord) return;

          this.setState({ searchLoading: true });
          const { ok, content } = await fetchList(PREFIX_LOCSEARCH, 1, "", "", {
            page_size: 1000,
            q: searchWord,
            longitude: mapRegion.longitude,
            latitude: mapRegion.latitude
          });
          if (ok) {
            const locations = content.map(item => ({
              ...item,
              description: item.result_type
            }));

            // next last fetch user coordinate will be undefine
            // as the query was not an existing user location query
            const lastFetchUserCoord = {
              latitude: -10000,
              longitude: -100000,
              latitudeDelta: 0.0,
              longitudeDelta: 0.0
            };
            this.setState({ lastFetchUserCoord, locations });
          }
          this.setState({ searchLoading: false });
        };

        render() {
          const {
            customLocations,
            detailLocation,
            searchWord,
            searchLoading,
            locations,
            mapRegion
          } = this.state;
          const { handleMarkerPress, intl, renderLocation } = this.props;

          // if there is search word, skip custom locations
          const concatLocations = searchWord
            ? locations
            : [...locations, ...customLocations];

          return (
            <Fragment>
              <MapBaseView
                style={{ height: "100%" }}
                // region={Object.keys(mapRegion).length > 2 ? mapRegion : null}
                // region={mapRegion.latitude < -180 ? null : mapRegion}
                onLongPress={this.handleMapLongPress}
                onRegionChange={this.handleMapRegionChange}
              >
                {concatLocations.map((location, idx) => (
                  <Marker
                    coordinate={{ ...location }}
                    description={location.description}
                    key={(location.name || "location") + "-" + idx}
                    onPress={() => handleMarkerPress(location)}
                    onCalloutPress={() => {
                      this.setState({ detailLocation: location });
                    }}
                    onDeselect={() => handleMarkerPress({})}
                    title={location.name}
                  />
                ))}
              </MapBaseView>
              <View
                style={{
                  alignItems: "flex-end",
                  flexDirection: "row",
                  height: 90,
                  justifyContent: "center",
                  left: 0,
                  position: "absolute",
                  right: 0,
                  top: 0
                }}
              >
                <MyModal
                  isVisible={Object.keys(detailLocation).length > 0}
                  handleModalClose={() => this.setState({ detailLocation: {} })}
                  title={detailLocation.name}
                >
                  {renderLocation(detailLocation)}
                </MyModal>
                <View style={{ height: 50, paddingBottom: 5, width: "100%" }}>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      width: "100%"
                    }}
                  >
                    <View style={{ width: "85%" }}>
                      <SearchBar
                        lightTheme
                        containerStyle={{
                          backgroundColor: "transparent",
                          borderBottomWidth: 0,
                          borderTopWidth: 0
                        }}
                        onChangeText={this.handleSearchChange}
                        onSubmitEditing={this.handleSearchSubmit}
                        placeholder={intl.formatMessage({
                          id: "bgtMapView.search"
                        })}
                        round
                        showLoading={searchLoading}
                        value={searchWord}
                      />
                    </View>
                  </View>
                </View>
              </View>
            </Fragment>
          );
        }
      }
    )
  )
);
