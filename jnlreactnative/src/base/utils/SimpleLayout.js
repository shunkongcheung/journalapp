import React, { Fragment } from "react";
import { ScrollView, View } from "react-native";

import { Title } from "./Title";

export const SimpleLayout = ({ children, title }) => (
  <Fragment>
    <Title height="15%" title={title} />
    <View style={{ backgroundColor: "#eee", height: "10%" }} />
    <View
      style={{
        borderColor: "#aaa",
        borderWidth: 2,
        backgroundColor: "white",
        height: "75%"
      }}
    >
      <ScrollView>{children}</ScrollView>
    </View>
  </Fragment>
);
