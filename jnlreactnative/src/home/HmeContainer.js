import React, { PureComponent } from "react";
import { Button, ScrollView, Text, View } from "react-native";
import { injectIntl } from "react-intl";

import {
  DEFAULT_DURATION,
  FadeView
} from "jnlreactnative/src/base/utils/FadeView";
import { HOCFetchList } from "../base/fetches/HOCFetchList";
import { HmeBarChartView } from "./HmeBarChartView";
import { InputResolver } from "../base/formView/InputResolver";
import {
  PREFIX_BGTMASTER,
  PREFIX_BGTCATAGORY
} from "../base/fetches/actionPrefixes";

export const HmeContainer = HOCFetchList(
  injectIntl(
    class HmeContainer extends PureComponent {
      state = {
        budgets: [],
        catagories: [],
        isRequestFinished: false,
        periodType: "weekly"
      };

      async componentDidMount() {
        await this.fetchBudgetsAndCatagories("weekly");
      }

      fetchBudgetsAndCatagories = async periodType => {
        const { fetchList } = this.props;
        const year = 2019;
        const q = { page_size: 10000 };

        this.setState({ budgets: [], isRequestFinished: false });

        const [budRes, catRes] = await Promise.all([
          fetchList(PREFIX_BGTMASTER, 1, "", "", {
            ...q,
            ...this.getQueryLtAndGte(periodType)
          }),
          this.state.catagories.length
            ? null
            : fetchList(PREFIX_BGTCATAGORY, 1, "", "", q)
        ]);

        if (catRes && catRes.ok) {
          const { content: catagories } = catRes;
          this.setState({ catagories });
        }

        if (budRes.ok) {
          let { content: budgets, options } = budRes;
          const catagories = catRes ? catRes.content : this.state.catagories;

          budgets = budgets.map(item => {
            const catName = catagories.find(cat => cat.id == item.catagory)
              .name;
            const mtdName = options.payment_method.choices.find(
              mtd => mtd.value == item.payment_method
            ).display_name;
            return { catName, mtdName, ...item };
          });
          this.setState({ budgets });
        }
        this.setState({ isRequestFinished: true });
      };

      getQueryLtAndGte = periodType => {
        let fd = new Date();
        let ld = new Date();

        switch (periodType) {
          case "weekly":
            fd.setDate(ld.getDate() - ld.getDay());
            ld.setDate(ld.getDate() - ld.getDay() + 6);
            break;
          case "monthly":
            fd = new Date(ld.getFullYear(), ld.getMonth(), 1);
            ld = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
            break;
          case "yearly":
            fd = new Date(ld.getFullYear(), 0, 1);
            ld = new Date(ld.getFullYear(), 11, 31);
        }

        const n = num => (num > 9 ? num : `0${num}`);
        return {
          created_at__lte: `${ld.getFullYear()}-${n(ld.getMonth() + 1)}-${n(
            ld.getDate()
          )}`,
          created_at__gte: `${fd.getFullYear()}-${n(fd.getMonth() + 1)}-${n(
            fd.getDate()
          )}`
        };
      };

      handlePeriodTypeChange = periodType => {
        this.setState({ periodType });
        this.fetchBudgetsAndCatagories(periodType);
      };
      render() {
        const { intl } = this.props;
        const {
          budgets,
          catagories,
          isRequestFinished,
          periodType
        } = this.state;

        return (
          <View
            style={{
              flex: 1,
              backgroundColor: "white",
              alignItems: "stretch"
            }}
          >
            <FadeView isVisible={true}>
              <ScrollView contentContainerStyle={{ marginBottom: 50 }}>
                <View style={{ alignItems: "center" }}>
                  <Text
                    style={{ fontWeight: "bold", fontSize: 20, margin: 40 }}
                  >
                    {intl.formatMessage({ id: "hmeContainer.welcome" })}
                  </Text>
                </View>
                <InputResolver
                  choices={[
                    {
                      display_name: intl.formatMessage({
                        id: "hmeContainer.filter.weekly"
                      }),
                      value: "weekly"
                    },
                    {
                      display_name: intl.formatMessage({
                        id: "hmeContainer.filter.monthly"
                      }),
                      value: "monthly"
                    },
                    {
                      display_name: intl.formatMessage({
                        id: "hmeContainer.filter.yearly"
                      }),
                      value: "yearly"
                    }
                  ]}
                  disabled={!isRequestFinished}
                  label=""
                  handleValueChange={this.handlePeriodTypeChange}
                  name="periodType"
                  value={periodType}
                  type="button"
                />
                <View style={{ marginBottom: 20 }}>
                  <HmeBarChartView
                    data={budgets.map(item => ({
                      title: item.catName,
                      value: item.amount
                    }))}
                    title={intl.formatMessage({
                      id: "hmeContainer.chartTitle.catagory"
                    })}
                  />
                </View>
                <View style={{ marginBottom: 20 }}>
                  <HmeBarChartView
                    data={budgets.map(item => ({
                      title: item.mtdName,
                      value: item.amount
                    }))}
                    title={intl.formatMessage({
                      id: "hmeContainer.chartTitle.paymentMethod"
                    })}
                  />
                </View>
              </ScrollView>
            </FadeView>
          </View>
        );
      }
    }
  )
);
