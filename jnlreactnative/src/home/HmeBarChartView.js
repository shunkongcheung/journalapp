import React, { Fragment, PureComponent } from "react";
import { injectIntl, intlShape } from "react-intl";
import PropTypes from "prop-types";
import {
  Animated,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  View
} from "react-native";
import isEqual from "lodash/isEqual";

const PADDING_HEIGHT = 100;
class HmeBarItem extends PureComponent {
  static propTypes = {
    barHeight: PropTypes.number.isRequired,
    duration: PropTypes.number,
    handlePress: PropTypes.func.isRequired,
    maxHeight: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired
  };
  height = new Animated.Value(0);
  marginTop = new Animated.Value(this.props.maxHeight);

  componentDidMount() {
    let { barHeight, duration, maxHeight } = this.props;
    duration = duration || 1000;
    Animated.parallel([
      Animated.timing(this.height, {
        toValue: barHeight,
        duration
      }),
      Animated.timing(this.marginTop, {
        toValue: maxHeight - barHeight,
        duration
      })
    ]).start();
  }

  render() {
    const { barHeight, handlePress, maxHeight, title } = this.props;
    return (
      <TouchableWithoutFeedback
        onLongPress={() => handlePress(title)}
        onPressOut={() => handlePress()}
      >
        <View
          style={{
            alignItems: "center",
            height: maxHeight + PADDING_HEIGHT
          }}
          onPress={this.handlePress}
        >
          <Animated.View
            style={{
              borderTopLeftRadius: 5,
              borderTopRightRadius: 5,
              backgroundColor: "#7F7CDC",
              height: this.height,
              marginLeft: 4,
              marginTop: this.marginTop,
              width: 50
            }}
          />
          <Text
            style={{
              marginTop: PADDING_HEIGHT * 0.2,
              transform: [{ rotate: "50deg" }]
            }}
          >
            {title}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export const HmeBarChartView = injectIntl(
  class HmeBarChartView extends PureComponent {
    static propTypes = {
      data: PropTypes.array.isRequired,
      intl: intlShape.isRequired,
      title: PropTypes.string.isRequired
    };

    state = { groupedData: [], touchedTitle: "" };

    handlePress = touchedTitle => this.setState({ touchedTitle });

    componentDidMount() {
      const { data } = this.props;
      this.setGroupedData();
    }

    componentDidUpdate() {
      const { groupedData } = this.state;
      this.setGroupedData();
    }

    setGroupedData = () => {
      const { data } = this.props;
      const groupedData = [];
      data.map(item => {
        const existData = groupedData.find(gd => gd.title == item.title);
        if (existData) existData.value += item.value;
        else groupedData.push({ ...item });
      });

      if (!isEqual(groupedData, this.state.groupedData)) {
        this.setState({ groupedData });
      }
    };

    render() {
      const { groupedData, touchedTitle } = this.state;
      const { intl, height = 400, title } = this.props;

      const total = groupedData.reduce((acc, item) => acc + item.value, 0);
      const max = groupedData.reduce(
        (acc, item) => (acc > item.value ? acc : item.value),
        0
      );
      const touchedItem = groupedData.find(item => item.title == touchedTitle);
      const touchedPercent = touchedItem
        ? (touchedItem.value * 100.0) / total
        : 0;

      return (
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            width: "100%"
          }}
        >
          <View
            style={{
              borderColor: "#DBDBE8",
              borderRadius: 5,
              borderWidth: 1,
              flexDirection: "row",
              justifyContent: "center",
              width: "80%",
              height: height + 2 + PADDING_HEIGHT
            }}
          >
            <ScrollView
              horizontal
              contentContainerStyle={{ justifyContent: "center" }}
            >
              {groupedData.map(item => (
                <HmeBarItem
                  barHeight={(height * 0.8 * item.value) / max}
                  handlePress={this.handlePress}
                  key={item.title}
                  maxHeight={height}
                  title={item.title}
                />
              ))}
            </ScrollView>

            <View
              style={{
                alignItems: "center",
                marginTop: 5,
                position: "absolute",
                top: 0,
                width: "100%"
              }}
            >
              <Text
                style={{ color: "black", fontWeight: "bold", fontSize: 18 }}
              >
                {title}
              </Text>
              {touchedItem ? (
                <Text>
                  {intl.formatMessage(
                    { id: "hmeBarChartView.info" },
                    {
                      value: touchedItem.value,
                      percent: touchedPercent,
                      title: touchedItem.title,
                      total
                    }
                  )}
                </Text>
              ) : null}
            </View>
          </View>
        </View>
      );
    }
  }
);
