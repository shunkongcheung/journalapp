import React, { PureComponent } from "react";

import { BgtItem } from "./BgtItem";
import { CalendarView } from "../base/utils/CalendarView";
import { HOCFetchList } from "../base/fetches/HOCFetchList";
import {
  PREFIX_BGTCATAGORY,
  PREFIX_BGTMASTER,
  PREFIX_LOCMASTER
} from "../base/fetches/actionPrefixes";

export const BgtCalendarView = HOCFetchList(
  class BgtCalendarView extends PureComponent {
    state = {
      budgets: [],
      catagories: [],
      locations: []
    };

    fetchItemsForMonth = async content => {
      const { fetchList } = this.props;
      const locationIds = content.reduce((acc, item) => {
        acc.push(item.location);
        return acc;
      }, []);
      const catagoryIds = content.reduce((acc, item) => {
        acc.push(item.catagory);
        return acc;
      }, []);

      const res = await Promise.all([
        fetchList(PREFIX_LOCMASTER, 1, "", "", {
          page_size: 10000,
          id__in: locationIds
        }),
        fetchList(PREFIX_BGTCATAGORY, 1, "", "", {
          page_size: 10000,
          id__in: catagoryIds
        })
      ]);
      if (res[0].ok) this.setState({ locations: res[0].content });
      if (res[1].ok) this.setState({ catagories: res[1].content });
    };

    render() {
      const { budgets, catagories, locations } = this.state;
      return (
        <CalendarView
          fetchPrefix={PREFIX_BGTMASTER}
          fetchItemsForMonth={this.fetchItemsForMonth}
          renderItem={({ item }) => (
            <BgtItem
              budget={item}
              catagories={catagories}
              locations={locations}
              isSkipDate
            />
          )}
        />
      );
    }
  }
);
