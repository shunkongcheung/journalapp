import React from "react";
import { Text, Platform, View } from "react-native";
import { injectIntl } from "react-intl";

export const BgtItem = injectIntl(
  ({ budget, catagories, isSkipLocation, isSkipDate, intl, locations }) => (
    <View style={{ marginBottom: 15 }}>
      <Text style={{ fontWeight: "bold", fontSize: 17, marginBottom: 10 }}>
        {budget.desc}
      </Text>

      <Text style={{ fontSize: 13, marginBottom: 5 }}>
        {intl.formatMessage(
          {
            id: "bgtItem.amount"
          },
          { amount: budget.amount }
        )}
      </Text>
      <Text style={{ color: "#777", fontSize: 10 }}>
        {catagories.length
          ? catagories.find(item => item.id == budget.catagory).name
          : null}
      </Text>
      {isSkipLocation ? null : (
        <Text style={{ color: "#88d", fontSize: 10 }}>
          {locations.length
            ? locations.find(item => item.id == budget.location).name
            : null}
        </Text>
      )}
      {isSkipDate ? null : (
        <Text style={{ color: "#88d", fontSize: 10 }}>
          {new Date(budget.created_at).toLocaleString()}
        </Text>
      )}
    </View>
  )
);
