import React, { Fragment, PureComponent } from "react";
import { injectIntl } from "react-intl";
import PropTypes from "prop-types";

import { BgtItem } from "./BgtItem";
import { HOCFetchList } from "../base/fetches/HOCFetchList";
import { MapView } from "../base/utils/MapView";
import {
  PREFIX_BGTMASTER,
  PREFIX_BGTCATAGORY
} from "../base/fetches/actionPrefixes";

export const BgtMapView = injectIntl(
  HOCFetchList(
    class BgtMapView extends PureComponent {
      static propTypes = {
        handleMarkerPress: PropTypes.func.isRequired
      };

      state = {
        budgets: [],
        catagories: []
      };

      getLocationDesc = location => {
        const { intl } = this.props;
        return intl.formatMessage(
          { id: "bgtMapView.locMaster.desc" },
          { times: location.budgets.length }
        );
      };

      renderLocation = detailLocation => {
        const { budgets, catagories } = this.state;

        return detailLocation.budgets
          ? detailLocation.budgets.map(item => (
              <BgtItem
                budget={budgets.find(bud => bud.id == item)}
                catagories={catagories}
                key={item}
                isSkipLocation
              />
            ))
          : null;
      };

      fetchFromLocations = async content => {
        const { fetchList } = this.props;

        const locationIds = content.reduce((acc, item) => {
          acc.push(item.location);
          return acc;
        }, []);

        const budgetIds = content.reduce((acc, item) => {
          acc.push(item.budgets);
          return acc;
        }, []);
        const res = await Promise.all([
          fetchList(PREFIX_BGTMASTER, 1, "", "", {
            page_size: 10000,
            id__in: budgetIds
          }),
          fetchList(PREFIX_BGTCATAGORY, 1, "", "", {
            page_size: 10000
          })
        ]);
        if (res[0].ok) this.setState({ budgets: res[0].content });
        if (res[1].ok) this.setState({ catagories: res[1].content });
      };

      render() {
        const { handleMarkerPress } = this.props;
        return (
          <MapView
            getLocationDesc={this.getLocationDesc}
            handleMarkerPress={handleMarkerPress}
            fetchFromLocations={this.fetchFromLocations}
            renderLocation={this.renderLocation}
            // renderItem={props => <BgtItem {...props} isSkipLocation />}
            userLocationQuery={{ budgets__isnull: false }}
          />
        );
      }
    }
  )
);
