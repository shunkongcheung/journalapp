import React, { PureComponent } from "react";
import { injectIntl } from "react-intl";
import * as yup from "yup";
import PropTypes from "prop-types";

import { FormView } from "../base/formView/FormView";
import { HOCFetchEdit } from "../base/fetches/HOCFetchEdit";
import {
  PREFIX_BGTCATAGORY,
  PREFIX_BGTMASTER,
  PREFIX_LOCMASTER
} from "../base/fetches/actionPrefixes";

export const BgtFormView = injectIntl(
  HOCFetchEdit(
    class BgtEditFormView extends PureComponent {
      static propTypes = {
        handleBack: PropTypes.func.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        selectedLocation: PropTypes.object.isRequired
        // options: PropTypes.object.isRequired
      };
      state = { options: {}, selectValues: [] };

      async componentDidMount() {
        // const { intl, options } = this.props;
        const { intl } = this.props;

        const { fetchOptions } = this.props;
        const { options } = await fetchOptions(PREFIX_BGTMASTER);

        const catagory = { ...options.catagory };
        delete options.catagory;
        options.location.read_only = true;

        options.locationName = {
          label: intl.formatMessage({
            id: "bgtFormView.locationNameLabel"
          }),
          required: true,
          type: "string"
        };
        options.isCreateCatagory = {
          choices: [
            {
              display_name: intl.formatMessage({
                id: "bgtFormView.chooseCat"
              }),
              value: 0
            },
            {
              display_name: intl.formatMessage({
                id: "bgtFormView.createCat"
              }),
              value: 1
            }
          ],
          label: "",
          required: true,
          type: "button"
        };
        options.catagoryName = ({ isCreateCatagory }) => ({
          label: intl.formatMessage({
            id: "bgtFormView.catagoryNameLabel"
          }),
          required: true,
          read_only: !isCreateCatagory,
          type: "string"
        });

        options.catagory = ({ isCreateCatagory }) => ({
          ...catagory,
          read_only: isCreateCatagory
        });

        this.setState({ options });
      }

      fetchCreateLocation = async data => {
        const { fetchSubmitCreate } = this.props;
        const { errors, content, ok } = await fetchSubmitCreate(
          PREFIX_LOCMASTER,
          data
        );
        return ok ? content.id : null;
      };

      fetchCreateCatagory = async data => {
        const { fetchSubmitCreate } = this.props;
        const { options } = this.state;

        const { errors, content, ok } = await fetchSubmitCreate(
          PREFIX_BGTCATAGORY,
          data
        );
        return ok ? content.id : null;
      };

      handleSubmit = async (data, apis) => {
        const {
          fetchSubmitCreate,
          handleSubmit,
          selectedLocation
        } = this.props;
        const { options } = this.state;

        data.location = await this.fetchCreateLocation({
          ...selectedLocation,
          name: data.locationName
        });

        if (data.isCreateCatagory)
          data.catagory = await this.fetchCreateCatagory({
            name: catagoryName
          });
        else data.catagory = data.catagory[0];

        data.payment_method = data.payment_method[0];

        const { errors, ok, content } = await fetchSubmitCreate(
          PREFIX_BGTMASTER,
          data
        );

        if (!ok) apis.setErrors(errors);
        else handleSubmit();
      };

      render() {
        const { handleBack, intl, selectedLocation } = this.props;
        const { options } = this.state;

        return (
          <FormView
            handleBack={handleBack}
            handleSubmit={this.handleSubmit}
            initialValues={{
              locationName: selectedLocation.name,
              isCreateCatagory: 0
            }}
            validationSchemaShape={{
              catagory: yup.mixed().when("isCreateCatagory", {
                is: 0,
                then: yup.array().required(),
                otherwise: yup.mixed()
              }),
              catagoryName: yup.mixed().when("isCreateCatagory", {
                is: 0,
                then: yup.mixed(),
                otherwise: yup.string().required()
              })
            }}
            title={intl.formatMessage({ id: "bgtFormView.title" })}
            options={options}
          />
        );
      }
    }
  )
);
