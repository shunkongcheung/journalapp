import React, { Fragment, PureComponent } from "react";
import { Alert, View } from "react-native";
import { Icon } from "react-native-elements";

import { BgtCalendarView } from "./BgtCalendarView";
import { BgtFormView } from "./BgtFormView";
import { BgtMapView } from "./BgtMapView";

import {
  DEFAULT_DURATION,
  FadeView
} from "jnlreactnative/src/base/utils/FadeView";
import { HOCFetchEdit } from "../base/fetches/HOCFetchEdit";
import {
  PREFIX_BGTCATAGORY,
  PREFIX_BGTMASTER,
  PREFIX_LOCMASTER
} from "../base/fetches/actionPrefixes";

const BgtNavigator = ({ handleAddPress, handleViewChange, viewName }) => (
  <View
    style={{
      position: "absolute",
      bottom: 5,
      right: 15
    }}
  >
    {viewName == "map" ? (
      <Fragment>
        <Icon name="add" raised onPress={handleAddPress} />
        <Icon
          name="perm-contact-calendar"
          raised
          onPress={() => handleViewChange("calendar")}
        />
      </Fragment>
    ) : null}
    {viewName == "calendar" ? (
      <Icon name="map" raised onPress={() => handleViewChange("map")} />
    ) : null}
  </View>
);

export const BgtContainer = HOCFetchEdit(
  class BgtContainer extends PureComponent {
    state = {
      options: {},
      selectedLocation: {},
      viewName: "map"
    };

    async componentDidMount() {
      const { fetchOptions } = this.props;
      const { errors, options } = await fetchOptions(PREFIX_BGTMASTER);
      this.setState({ errors, options });
    }

    handleBack = () => {
      this.setState({ selectedLocation: {} });
      this.handleViewChange("map");
    };

    handleAddPress = () => {
      const { selectedLocation } = this.state;
      if (Object.keys(selectedLocation).length) {
        this.handleViewChange("create");
      } else {
        Alert.alert("Please select a location.");
      }
    };

    handleViewChange = viewName => {
      this.setState({ viewName: "" });
      setTimeout(() => this.setState({ viewName }), DEFAULT_DURATION);
    };

    render() {
      const { options, selectedLocation, viewName } = this.state;
      return (
        <View style={{ backgroundColor: "white", flex: 1 }}>
          <FadeView
            isVisible={viewName == "calendar"}
            style={{ height: "100%" }}
          >
            <BgtCalendarView
              handleMarkerPress={selectedLocation =>
                this.setState({ selectedLocation: selectedLocation || {} })
              }
            />
          </FadeView>
          <FadeView isVisible={viewName == "create"} style={{ height: "100%" }}>
            <BgtFormView
              handleBack={this.handleBack}
              handleSubmit={() => this.handleViewChange("map")}
              selectedLocation={selectedLocation}
              options={options}
            />
          </FadeView>
          <FadeView isVisible={viewName == "map"} style={{ height: "100%" }}>
            <BgtMapView
              handleMarkerPress={selectedLocation =>
                this.setState({ selectedLocation: selectedLocation || {} })
              }
            />
          </FadeView>
          <BgtNavigator
            handleAddPress={this.handleAddPress}
            handleViewChange={this.handleViewChange}
            viewName={viewName}
          />
        </View>
      );
    }
  }
);
