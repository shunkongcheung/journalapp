import React, { PureComponent } from "react";
import { Platform } from "react-native";
import { Provider } from "react-redux";
import { createBottomTabNavigator, createAppContainer } from "react-navigation";
import Ionicons from "react-native-vector-icons/Ionicons";

import { BgtContainer } from "./budget/BgtContainer";
import { HmeContainer } from "./home/HmeContainer";
import { GlobalConfigurator, store } from "./base/global/GlobalConfigurator";
import { LangProvider } from "./base/language/LangProvider";
import { SetContainer } from "./settings/SetContainer";

if (Platform.OS === "android") {
  // only android needs polyfill
  require("intl"); // import intl object
  require("intl/locale-data/jsonp/en-IN"); // load the required locale details
}

const TabNavigator = createBottomTabNavigator(
  {
    Home: HmeContainer,
    Budget: BgtContainer,
    Settings: SetContainer
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName = "md-information";
        switch (routeName) {
          case "Home":
            iconName = "md-home";
            break;
          case "Budget":
            iconName = "md-basket";
            break;
          case "Settings":
            iconName = "ios-hammer";
            break;
        }
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "tomato",
      inactiveTintColor: "gray"
    }
  }
);

const NavigatorContainer = createAppContainer(TabNavigator);

const App = props => (
  <Provider store={store}>
    <LangProvider>
      <GlobalConfigurator>
        <NavigatorContainer {...props} />
      </GlobalConfigurator>
    </LangProvider>
  </Provider>
);

export default App;

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import Main from "./Main";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
        <Main />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
 */
