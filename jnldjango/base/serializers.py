from django.utils import timezone
from rest_framework.serializers import (
    DateTimeField, IntegerField,
    ModelSerializer, SlugRelatedField,
)


class LocalTime(DateTimeField):
    def to_representation(self, value):
        return timezone.localtime(value)


class MyModelSerializer(ModelSerializer):
    created_by = SlugRelatedField(read_only=True, slug_field='username')
    created_at = LocalTime(read_only=True)
    modified_by = SlugRelatedField(read_only=True, slug_field='username')
    modified_at = LocalTime(read_only=True)

    def __init__(self, *args, **kwargs):
        is_listview = kwargs.pop("is_listview", False)
        meta = getattr(self, 'Meta', None)
        updated_fields = meta.fields
        if hasattr(meta, "object_fields"):
            if not is_listview:
                updated_fields = meta.fields + meta.object_fields
            else:
                updated_fields = [
                    item for item in meta.fields
                    if item not in meta.object_fields
                ]
        setattr(
            meta, "fields", updated_fields +
            ["created_by",  "created_at", "modified_by", "modified_at"]
        )

        super(MyModelSerializer, self).__init__(*args, **kwargs)

        self.fields["id"] = IntegerField(label='ID', read_only=True)
