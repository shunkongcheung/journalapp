from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class MyBaseModel(models.Model):
    enable = models.BooleanField(default=True)
    created_by = models.ForeignKey(
        User, related_name="+", on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True)

    modified_by = models.ForeignKey(
        User, related_name="+", on_delete=models.CASCADE
    )
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
