from django.core.management.base import BaseCommand, CommandError
from general.models import GnlLookUpTable


class Command(BaseCommand):
    help = 'create or update lookup tables'

    def add_arguments(self, parser):
        pass

    def create_or_update_lookup(self, lookup_template):
        query = GnlLookUpTable.objects.filter(
            group=lookup_template["group"],
            key=lookup_template["key"]
        )
        if query.count():
            query.update(**lookup_template)
            return True
        else:
            lookup_object = GnlLookUpTable.objects.create(**lookup_template)
            lookup_object.save()
            return False

    def get_lookup_list(self):
        return [
            {"group": "LOCATION_SEARCH", "key": "APP_ID",
                "value": "eOPvKxparmxuVtJzsMwG"},
            {"group": "LOCATION_SEARCH", "key": "APP_CODE",
                "value": "jHjwQ56aso8VorT6IGSZfg"},


            {"group": "USER_CREATE_MAIL", "key": "SUBJECT",
                "value": "Welcome to journal app"},
            {"group": "USER_CREATE_MAIL", "key": "MESSAGE",
                "value": '''Dear {first_name},

We are delighted to have you joining us.
Please login with the following account:
username: {username},
password: {password}

Cheers,
Journalapp Administrator
                '''},

            {"group": "USER_PASSWORD_RESET_MAIL", "key": "SUBJECT",
                "value": "Journal app password rest"},
            {"group": "USER_PASSWORD_RESET_MAIL", "key": "MESSAGE",
                "value": '''Dear {first_name},

We have reset your password.
You may update your password after login.
username: {username},
password: {password}

Cheers,
Journalapp Administrator
                '''},


            {"group": "GENERAL_MAIL", "key": "SENDER",
                "value": " Journal App Administrator"},
        ]

    def handle(self, *args, **options):
        lookup_list = self.get_lookup_list()

        for idx, lookup_template in enumerate(lookup_list):
            exist = self.create_or_update_lookup(lookup_template)
            status = "updating" if exist else "creating"
            print("{:<5}: {} lookup - [{}: {}]".format(
                str(idx),
                status,
                lookup_template["group"],
                lookup_template["key"],

            ))
