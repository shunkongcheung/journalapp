from django.contrib import admin

# Register your models here.


class MyBaseAdmin(admin.ModelAdmin):
    def __init__(self, *args, **kwargs):
        self.list_display = self.list_display + \
            ["created_by", "created_at", "modified_by", "modified_at"]
        return super(MyBaseAdmin, self).__init__(*args, **kwargs)
