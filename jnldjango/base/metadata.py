from __future__ import absolute_import
from rest_framework.metadata import SimpleMetadata
from django.utils.translation import gettext_lazy as _
from django.utils.encoding import force_text

# import logging
# logger = logging.getLogger(__name__)


class MyRootMetadata(SimpleMetadata):
    def determine_metadata(self, request, view):
        self.user = request.user
        meta = super(MyRootMetadata, self).determine_metadata(request, view)
        if (hasattr(view, "list") or hasattr(view, "retrieve")) \
                and hasattr(view, 'get_serializer'):
            serializer = view.get_serializer()
            meta["mapping"] = self.get_serializer_info(serializer)
        return meta

    def get_filtered_queryset(self, queryset, user):
        return queryset.filter(created_by=self.user, enable=True)

    def get_serializer_info(self, serializer):
        for field_name, field in serializer.fields.items():
            class_name = field.__class__.__name__
            try:
                if class_name == 'PrimaryKeyRelatedField' and \
                        hasattr(field, "queryset") and hasattr(field.queryset, "model"):
                    field.queryset = self.get_filtered_queryset(field.queryset)

                if class_name == "ManyRelatedField" and hasattr(field, "child_relation") and \
                        field.child_relation.__class__.__name__ == "PrimaryKeyRelatedField":

                    if not hasattr(field.child_relation, "read_only") or not field.child_relation.read_only:
                        field.child_relation.queryset = self.get_filtered_queryset(
                            field.child_relation.queryset)

            except Exception as ex:
                pass
                # logger.error(
                #     """MyRootMetadata get_serializer_info():  field [{}]""".format(field_name) +
                #     """ is a ManyRelatedField but does not have a queryset. The child_relation is [{}] - """.format(
                #         class_name
                #     ) + str(ex)
                # )
        return super(MyRootMetadata, self).get_serializer_info(serializer)

    def get_field_info(self, field):
        field_info = super(MyRootMetadata, self).get_field_info(field)

        if field.style.get('input_type', None) is not None:
            field_info['input_type'] = field.style['input_type']
        if self.label_lookup[field] == 'field':
            field_info['multiple'] = field.__class__.__name__ == 'ManyRelatedField'

            if hasattr(field, "choices"):
                field_info['choices'] = [
                    {
                        'value': choice_value,
                        'display_name': force_text(choice_name, strings_only=True),
                    }
                    for choice_value, choice_name in field.choices.items()
                ]
                field_info['type'] = 'choice'

        def isEnglish(s):
            try:
                s.encode(encoding='utf-8').decode('ascii')
                return True
            except UnicodeDecodeError:
                return False

        if "label" in field_info and isEnglish(field_info["label"]):
            field_info["label"] = _(field_info["label"].lower())

        return field_info
