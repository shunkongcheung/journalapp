from __future__ import absolute_import

from copy import deepcopy

from django.contrib.auth.models import User
from django.core.serializers.json import DjangoJSONEncoder
from django.utils import timezone

from rest_framework.generics import (
    CreateAPIView, GenericAPIView,
    ListAPIView,
)
from base.metadata import MyRootMetadata
from general.models import GnlAuditTrail
from rest_framework.mixins import (
    DestroyModelMixin, RetrieveModelMixin,
    UpdateModelMixin,
)

import json


class MyBaseAPIView(GenericAPIView):
    metadata_class = MyRootMetadata

    def base_wrapper(self, base_function, request, *args, **kwargs):
        ex_item = None
        action_type, level = base_function.__name__, GnlAuditTrail.LEV_INFO
        start_time, response_data = timezone.now(), None

        request_data = request.data
        request_queries = deepcopy(request.query_params)
        if len(args) > 1:
            request_queries.update(args[1])

        try:
            response = base_function(request, *args, **kwargs)
            response_data = json.loads(json.dumps(
                response.data, cls=DjangoJSONEncoder
            ))
        except Exception as ex:
            level, response_data = GnlAuditTrail.LEV_ERROR, str(ex)
            ex_item = ex

        self.create_audit_trail(
            action_type, level, start_time,
            request_data, request_queries,
            response_data
        )

        if ex_item is not None:
            raise ex_item

        return response

    def create_audit_trail(
        self, action_type, level, start_time,
        request_data, request_queries,
        response_data
    ):
        user = self.request.user
        if user.is_anonymous:
            user = User.objects.get(username="admin")

        audit_trail = GnlAuditTrail.objects.create(
            class_name=self.__class__.__name__, action_type=action_type,
            level=level, start_time=start_time,
            request_data=request_data, request_queries=request_queries,
            response_data=response_data,
            created_by=user,
            modified_by=user
        )
        audit_trail.save()

    def get_queryset(self):
        return self.model.objects.filter(created_by=self.request.user, enable=True)

    def options(self, request, *args, **kwargs):
        return self.base_wrapper(super(MyBaseAPIView, self).options, request, *args, **kwargs)


class MyListAPIView(ListAPIView, MyBaseAPIView):
    filter_fields = ["name__icontains"]

    def list(self, request, *args, **kwargs):
        return super(MyListAPIView, self).base_wrapper(super(MyListAPIView, self).list, request, *args, **kwargs)

    def get_filter_fields(self):
        return self.filter_fields + [
            "created_by__username__icontains",
            "modified_by__username__icontains",
        ]

    def get_serializer(self, *args, **kwargs):
        kwargs["is_listview"] = True
        return super(MyListAPIView, self).get_serializer(*args, **kwargs)

    def filter_queryset(self, queryset):
        filter_by = self.request.query_params.get("filter_by", None)
        order_by = self.request.query_params.get('order_by', 'modified_at')

        # this is doing and join
        query_params_filter_dict, is_filter_dict = {}, False
        for key in self.request.query_params.keys():
            value = self.request.query_params[key]
            if value and key != "filter_by" and key != "order_by" and key != "page" and key != "page_size":
                query_params_filter_dict[key] = value if not key.endswith(
                    "in") else value.split(",")
                if query_params_filter_dict[key] == "true":
                    query_params_filter_dict[key] = True
                if query_params_filter_dict[key] == "false":
                    query_params_filter_dict[key] = False

                is_filter_dict = True

        query_parms_filtered = queryset.filter(
            **query_params_filter_dict) if is_filter_dict else queryset.none()

        # this is doing or join
        internal_filtered = queryset.none()
        if filter_by:
            for filter_field in self.get_filter_fields():
                internal_filter_dict = {}
                internal_filter_dict[filter_field] = filter_by
                internal_filtered = internal_filtered | \
                    queryset.filter(**internal_filter_dict)

        # get unordered dict. See if any filter were applied
        is_filtered = filter_by or is_filter_dict
        unorder_queryset = (query_parms_filtered | internal_filtered).distinct()\
            if is_filtered else queryset

        # return ordered queryset
        return unorder_queryset.order_by(order_by) if order_by else unorder_queryset


class MyCreateAPIView(CreateAPIView, MyBaseAPIView):

    def create(self, request, *args, **kwargs):
        return super(MyCreateAPIView, self).base_wrapper(super(MyCreateAPIView, self).create, request, *args, **kwargs)

    # def get_serializer(self, *args, **kwargs):
    #     if hasattr(self, "request") and self.request is not None:
    #         kwargs["user"] = self.request.user
    #     return super(MyCreateAPIView, self).get_serializer(*args, **kwargs)

    def perform_create(self, serializer):
        serializer.validated_data['created_by'] = self.request.user
        serializer.validated_data['modified_by'] = self.request.user
        serializer.save()


class MyObjectAPIView(UpdateModelMixin, RetrieveModelMixin, DestroyModelMixin, MyBaseAPIView):
    is_using_mymodel_serializer = True

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, args, kwargs)

    def destroy(self, request, *args, **kwargs):
        return super(MyObjectAPIView, self).base_wrapper(super(MyObjectAPIView, self).destroy, request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, args, kwargs)

    # def get_serializer(self, *args, **kwargs):
    #     if self.is_using_mymodel_serializer and hasattr(self, "request") and self.request is not None:
    #         kwargs["user"] = self.request.user
    #     return super(MyObjectAPIView, self).get_serializer(*args, **kwargs)

    def perform_update(self, serializer):
        serializer.instance.modified_by = self.request.user
        serializer.save()

    def perform_destroy(self, instance):
        if getattr(instance, 'enable', None) is not None:
            instance.enable = False
        elif getattr(instance, 'is_active', None) is not None:
            instance.is_active = False

        instance.modified_by = self.request.user
        instance.save()

    def put(self, request, *args, **kwargs):
        return self.update(request, args, kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super(MyObjectAPIView, self).base_wrapper(super(MyObjectAPIView, self).retrieve, request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super(MyObjectAPIView, self).base_wrapper(super(MyObjectAPIView, self).update, request, *args, **kwargs)
