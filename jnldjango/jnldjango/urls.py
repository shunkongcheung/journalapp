"""jnldjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path
from django.views.generic import TemplateView

from rest_framework_swagger.views import get_swagger_view

from rest_framework_jwt.views import (
    obtain_jwt_token,
    refresh_jwt_token,
)

urlpatterns = [
    url(r'api/budget/', include("budget.urls")),
    url(r'api/event/', include("event.urls")),
    url(r'api/general/', include("general.urls")),
    url(r'api/location/', include("location.urls")),
    url(r'api/user_account/', include("user_account.urls")),

    path('api/token/obtain/', obtain_jwt_token),
    path('api/token/refresh/', refresh_jwt_token),

    path('admin/', admin.site.urls),
    path('docs/', get_swagger_view(title='Journal App APIs')),
    path('', TemplateView.as_view(template_name='index.html')),
]
