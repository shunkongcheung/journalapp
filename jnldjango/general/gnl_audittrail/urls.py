from django.conf.urls import url

from .apis import (
    GnlAuditTrailListAPIView,
    GnlAuditTrailObjectAPIView,
)

urlpatterns = [
    url(r"^list/$", GnlAuditTrailListAPIView.as_view()),
    url(r"^(?P<pk>[0-9]+)/$", GnlAuditTrailObjectAPIView.as_view()),
]
