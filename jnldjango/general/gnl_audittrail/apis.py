from base.apis import (
    MyListAPIView,
    MyObjectAPIView,
)
from base.serializers import MyModelSerializer
from general.models import GnlAuditTrail


class GnlAuditTrailSerializer(MyModelSerializer):
    class Meta:
        fields = ["class_name",  "action_type", "level"]
        object_fields = ["request_data", "response_data"]
        model = GnlAuditTrail


class GnlAuditTrailListAPIView(MyListAPIView):
    model = GnlAuditTrail
    serializer_class = GnlAuditTrailSerializer


class GnlAuditTrailObjectAPIView(MyObjectAPIView):
    http_methods = ["get", "options"]
    model = GnlAuditTrail
    serializer_class = GnlAuditTrailSerializer
