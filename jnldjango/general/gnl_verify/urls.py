from django.conf.urls import url

from .apis import (
    GnlVerifyDatabaseAPIView,
)

urlpatterns = [
    url(r"^database/$", GnlVerifyDatabaseAPIView.as_view()),
]
