from base.apis import (
    MyObjectAPIView,
)
from django.db import IntegrityError
from general.models import GnlLookUpTable

from rest_framework.serializers import (
    CharField,
    Serializer,
)
from rest_framework.response import Response
from rest_framework.views import APIView


class GnlVerifySerializer(Serializer):
    information = CharField(read_only=True)


class GnlVerifyDatabaseAPIView(APIView):
    http_methods = ["get", "options"]
    serializer_class = GnlVerifySerializer

    def get(self, request, *args, **kwargs):
        try:
            GnlLookUpTable.objects.first()
            return Response({"information": "database is connected."})
        except Exception as ex:
            print("exception ...", ex)
            return Response({"information": "database may not be connected."})
