# Generated by Django 2.1.5 on 2019-01-30 13:57

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0007_gnlaudittrail_request_queries'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gnlaudittrail',
            name='request_data',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True),
        ),
        migrations.AlterField(
            model_name='gnlaudittrail',
            name='request_queries',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True),
        ),
        migrations.AlterField(
            model_name='gnlaudittrail',
            name='response_data',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True),
        ),
    ]
