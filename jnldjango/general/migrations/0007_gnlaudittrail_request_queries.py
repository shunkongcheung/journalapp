# Generated by Django 2.1.5 on 2019-01-30 13:56

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0006_remove_gnlaudittrail_end_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='gnlaudittrail',
            name='request_queries',
            field=django.contrib.postgres.fields.jsonb.JSONField(default={}),
            preserve_default=False,
        ),
    ]
