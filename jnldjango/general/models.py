from base.models import MyBaseModel

from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import gettext_lazy as _

# Create your models here.


class GnlAuditTrail(MyBaseModel):
    LEV_ERROR = "ERROR"
    LEV_INFO = "INFO"

    class_name = models.CharField(max_length=128)
    action_type = models.CharField(max_length=64)
    level = models.CharField(
        max_length=32,
        choices=[(LEV_INFO, _("Info")), (LEV_ERROR, _("Error"))],
        default=LEV_INFO
    )
    start_time = models.DateTimeField()

    request_data = JSONField(blank=True)
    request_queries = JSONField(blank=True)

    response_data = JSONField(blank=True)

    def __str__(self):
        return "{}: {}".format(self.class_name, self.action_type)


class GnlLookUpTable(models.Model):
    group = models.CharField(max_length=128)
    key = models.CharField(max_length=128)
    value = models.TextField()

    class Meta:
        unique_together = (("group", "key"))

    @classmethod
    def get_lookup_value(cls, group, key):
        query = cls.objects.filter(group=group, key=key)
        return None if query.count() != 1 else query.first().value

    def __str__(self):
        return "{:50} - {}".format(self.group, self.key)
