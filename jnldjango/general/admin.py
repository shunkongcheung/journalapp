from django.contrib import admin
from .models import (
    GnlAuditTrail,
    GnlLookUpTable,
)


class GnlAuditTrailAdmin(admin.ModelAdmin):
    list_display = [
        "class_name", "action_type", "level",
        "created_by",  "start_time", "created_at",
    ]

    search_fields = ["class_name", "action_type", "level"]


class GnlLookUpTableAdmin(admin.ModelAdmin):
    list_display = ["group",  "key", "value"]
    search_fields = ["group",  "key", "value"]


# Register your models here.
admin.site.register(GnlAuditTrail, GnlAuditTrailAdmin)
admin.site.register(GnlLookUpTable, GnlLookUpTableAdmin)
