from django.conf.urls import url, include

from .gnl_audittrail import urls as gnl_audittrail_urls
from .gnl_verify import urls as gnl_verify_urls

app_name = "general"

urlpatterns = [
    url(r"gnl_audittrail/", include(gnl_audittrail_urls)),
    url(r"gnl_verify/", include(gnl_verify_urls)),
]
