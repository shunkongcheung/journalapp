from base.apis import (
    MyCreateAPIView,
    MyObjectAPIView,
)
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.utils.translation import gettext as _
from general.models import GnlLookUpTable
from rest_framework.serializers import (
    CharField,
    ModelSerializer,
    Serializer,
    ValidationError
)


class GnlUserSerializer(ModelSerializer):
    class Meta:
        fields = ["id", "username",  "first_name", "last_name", "email"]
        required_fields = ["username", "first_name", "email"]
        extra_kwargs = {field: {'required': True} for field in required_fields}
        model = User


class GnlUserPasswordResetSerializer(Serializer):
    username = CharField(required=True)
    email = CharField(required=True)

    class Meta:
        fields = ["username", "email"]

    def validate(self, values):
        if User.objects.filter(**values).count() != 1:
            raise ValidationError(
                _("User does not correspond to any existing user")
            )
        return values


def send_email_to_user(group, password, user):
    subject = GnlLookUpTable.objects.get(group=group, key="SUBJECT").value
    message = GnlLookUpTable.objects.get(group=group, key="MESSAGE").value.format(
        first_name=user.first_name,
        password=password,
        username=user.username
    )
    sender = GnlLookUpTable.objects.get(
        group="GENERAL_MAIL", key="SENDER").value
    receipient = user.email

    send_mail(subject, message, sender, [receipient], fail_silently=False)


class GnlUserCreateAPIView(MyCreateAPIView):
    permission_classes = []
    model = User
    serializer_class = GnlUserSerializer

    def perform_create(self, serializer):
        serializer.save()
        password = User.objects.make_random_password()
        serializer.instance.set_password(password)
        serializer.instance.save()

        group = "USER_CREATE_MAIL"
        send_email_to_user(group, password, serializer.instance)


class GnlUserObjectAPIView(MyObjectAPIView):
    model = User
    serializer_class = GnlUserSerializer
    http_method_names = ["get", "put", "options"]

    def get_object(self):
        return self.request.user

    def get_serializer(self, *args, **kwargs):
        serializer = super(
            GnlUserObjectAPIView, self
        ).get_serializer(*args, **kwargs)

        serializer.fields["password"] = CharField(
            style={'input_type': 'password'}, write_only=True, required=False)
        serializer.fields["username"] = CharField(read_only=True)

        return serializer

    def perform_update(self, serializer):
        super(GnlUserObjectAPIView, self).perform_update(serializer)
        password = serializer.validated_data.get("password")
        if password:
            serializer.instance.set_password(password)
            serializer.instance.save()


class GnlUserPasswordResetAPIView(MyCreateAPIView):
    permission_classes = []
    model = User
    serializer_class = GnlUserPasswordResetSerializer

    def perform_create(self, serializer):
        user = User.objects.get(**serializer.validated_data)
        password = User.objects.make_random_password()
        user.set_password(password)
        user.save()
        group = "USER_PASSWORD_RESET_MAIL"
        send_email_to_user(group, password, user)
