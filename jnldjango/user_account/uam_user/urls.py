from django.conf.urls import url

from .apis import (
    GnlUserCreateAPIView,
    GnlUserObjectAPIView,
    GnlUserPasswordResetAPIView,
)

urlpatterns = [
    url(r"^create/$", GnlUserCreateAPIView.as_view()),
    url(r"^self/$", GnlUserObjectAPIView.as_view()),
    url(r"^pwd_reset/$", GnlUserPasswordResetAPIView.as_view()),
]
