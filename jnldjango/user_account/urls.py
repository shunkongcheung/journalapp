from django.conf.urls import url, include

from .uam_user import urls as uam_user_urls

app_name = "user_account"

urlpatterns = [
    url(r"uam_user/", include(uam_user_urls)),
]
