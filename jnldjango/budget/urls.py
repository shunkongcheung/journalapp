from django.conf.urls import url, include

from .bgt_catagory import urls as bgt_catagory_urls
from .bgt_master import urls as bgt_master_urls

app_name = "budget"

urlpatterns = [
    url(r"bgt_catagory/", include(bgt_catagory_urls)),
    url(r"bgt_master/", include(bgt_master_urls)),
]
