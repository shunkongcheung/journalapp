from base.models import MyBaseModel
from django.db import models
from django.utils.translation import gettext as _
from location.models import LocMaster


# Create your models here.


class BgtCatagory(MyBaseModel):
    name = models.CharField(max_length=256)

    def __str__(self):
        return "{}".format(self.name)


class BgtMaster(MyBaseModel):
    PMT_CASH = ("CASH", _("cash"))
    PMT_OCTOPUS = ("OCTOPUS", _("octopus"))
    PMT_PAYME = ("PAYME", _("payme"))
    PMT_CREDIT = ("CREDIT", _("credit card"))

    amount = models.FloatField()
    desc = models.CharField(max_length=256)
    catagory = models.ForeignKey(BgtCatagory, on_delete=models.CASCADE)
    location = models.ForeignKey(
        LocMaster, on_delete=models.CASCADE, related_name="budgets"
    )

    payment_method = models.CharField(choices=[
        PMT_CASH, PMT_OCTOPUS, PMT_PAYME, PMT_CREDIT
    ],  max_length=32)

    def __str__(self):
        return "{}: {}".format(self.catagory.name, self.desc)
