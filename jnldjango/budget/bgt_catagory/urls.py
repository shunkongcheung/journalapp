from django.conf.urls import url

from .apis import (
    BgtCatagoryCreateAPIView,
    BgtCatagoryListAPIView,
    BgtCatagoryObjectAPIView,
)

urlpatterns = [
    url(r"^create/$", BgtCatagoryCreateAPIView.as_view()),
    url(r"^list/$", BgtCatagoryListAPIView.as_view()),
    url(r"^(?P<pk>[0-9]+)/$", BgtCatagoryObjectAPIView.as_view()),
]
