from base.apis import (
    MyCreateAPIView,
    MyListAPIView,
    MyObjectAPIView,
)
from base.serializers import MyModelSerializer
from budget.models import BgtCatagory


class BgtCatagorySerializer(MyModelSerializer):
    class Meta:
        fields = ["name"]
        model = BgtCatagory


class BgtCatagoryCreateAPIView(MyCreateAPIView):
    model = BgtCatagory
    serializer_class = BgtCatagorySerializer


class BgtCatagoryListAPIView(MyListAPIView):
    model = BgtCatagory
    serializer_class = BgtCatagorySerializer


class BgtCatagoryObjectAPIView(MyObjectAPIView):
    model = BgtCatagory
    serializer_class = BgtCatagorySerializer
