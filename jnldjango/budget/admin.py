from base.admin import MyBaseAdmin
from django.contrib import admin
from .models import (
    BgtCatagory,
    BgtMaster,
)


class BgtCatagoryAdmin(MyBaseAdmin):
    list_display = ["name"]
    search_fields = ["name"]


class BgtMasterAdmin(MyBaseAdmin):
    list_display = ["desc", "amount", ]
    search_fields = ["desc", "amount", ]


# Register your models here.
admin.site.register(BgtCatagory, BgtCatagoryAdmin)
admin.site.register(BgtMaster, BgtMasterAdmin)
