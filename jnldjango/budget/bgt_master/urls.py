from django.conf.urls import url

from .apis import (
    BgtMasterCreateAPIView,
    BgtMasterListAPIView,
    BgtMasterObjectAPIView,
)

urlpatterns = [
    url(r"^create/$", BgtMasterCreateAPIView.as_view()),
    url(r"^list/$", BgtMasterListAPIView.as_view()),
    url(r"^(?P<pk>[0-9]+)/$", BgtMasterObjectAPIView.as_view()),
]
