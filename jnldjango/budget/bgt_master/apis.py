from base.apis import (
    MyCreateAPIView,
    MyListAPIView,
    MyObjectAPIView,
)
from base.serializers import MyModelSerializer
from budget.models import BgtMaster


class BgtMasterSerializer(MyModelSerializer):
    class Meta:
        fields = ["amount", "desc", "catagory", "location", "payment_method"]
        model = BgtMaster


class BgtMasterCreateAPIView(MyCreateAPIView):
    model = BgtMaster
    serializer_class = BgtMasterSerializer


class BgtMasterListAPIView(MyListAPIView):
    model = BgtMaster
    serializer_class = BgtMasterSerializer


class BgtMasterObjectAPIView(MyObjectAPIView):
    model = BgtMaster
    serializer_class = BgtMasterSerializer
