from base.apis import (
    MyCreateAPIView,
    MyListAPIView,
    MyObjectAPIView,
)
from base.serializers import MyModelSerializer
from event.models import EvtMaster


class EvtMasterSerializer(MyModelSerializer):
    class Meta:
        fields = ["name", "start_date", "end_date"]
        model = EvtMaster


class EvtMasterCreateAPIView(MyCreateAPIView):
    model = EvtMaster
    serializer_class = EvtMasterSerializer


class EvtMasterListAPIView(MyListAPIView):
    model = EvtMaster
    serializer_class = EvtMasterSerializer


class EvtMasterObjectAPIView(MyObjectAPIView):
    model = EvtMaster
    serializer_class = EvtMasterSerializer
