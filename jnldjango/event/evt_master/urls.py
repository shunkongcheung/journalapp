from django.conf.urls import url

from .apis import (
    EvtMasterCreateAPIView,
    EvtMasterListAPIView,
    EvtMasterObjectAPIView,
)

urlpatterns = [
    url(r"^create/$", EvtMasterCreateAPIView.as_view()),
    url(r"^list/$", EvtMasterListAPIView.as_view()),
    url(r"^(?P<pk>[0-9]+)/$", EvtMasterObjectAPIView.as_view()),
]
