from base.models import MyBaseModel
from budget.models import BgtMaster
from django.db import models
from location.models import LocMaster

# Create your models here.


class EvtMaster(MyBaseModel):
    name = models.CharField(max_length=512)
    start_date = models.DateField()
    end_date = models.DateField()


class EvtNote(MyBaseModel):
    content = models.TextField(default="", blank=True)
    date = models.DateField()

    event = models.ForeignKey(EvtMaster, on_delete=models.CASCADE)
    budgets = models.ManyToManyField(BgtMaster, blank=True)
    locations = models.ManyToManyField(LocMaster, blank=True)

    class Meta:
        unique_together = (("date", "event"), )

    def __str__(self):
        return self.date.strftime('%Y-%m-%d')
