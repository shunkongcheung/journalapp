from base.admin import MyBaseAdmin
from django.contrib import admin
from .models import EvtMaster, EvtNote


class EvtMasterAdmin(MyBaseAdmin):
    list_display = ["name", "start_date", "end_date"]


class EvtNoteAdmin(MyBaseAdmin):
    list_display = ["date"]


# Register your models here.
admin.site.register(EvtMaster, EvtMasterAdmin)
admin.site.register(EvtNote, EvtNoteAdmin)
