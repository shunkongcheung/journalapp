from django.conf.urls import url

from .apis import (
    EvtNoteCreateAPIView,
    EvtNoteListAPIView,
    EvtNoteObjectAPIView,
)

urlpatterns = [
    url(r"^create/$", EvtNoteCreateAPIView.as_view()),
    url(r"^list/$", EvtNoteListAPIView.as_view()),
    url(r"^(?P<pk>[0-9]+)/$", EvtNoteObjectAPIView.as_view()),
]
