from base.apis import (
    MyCreateAPIView,
    MyListAPIView,
    MyObjectAPIView,
)
from base.serializers import MyModelSerializer
from event.models import EvtNote


class EvtNoteSerializer(MyModelSerializer):
    class Meta:
        fields = ["date", "content", "event"]
        model = EvtNote


class EvtNoteCreateAPIView(MyCreateAPIView):
    model = EvtNote
    serializer_class = EvtNoteSerializer


class EvtNoteListAPIView(MyListAPIView):
    model = EvtNote
    serializer_class = EvtNoteSerializer


class EvtNoteObjectAPIView(MyObjectAPIView):
    model = EvtNote
    serializer_class = EvtNoteSerializer
