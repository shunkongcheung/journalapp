from django.conf.urls import url, include

from .evt_master import urls as evt_master_urls
from .evt_note import urls as evt_note_urls

app_name = "event"

urlpatterns = [
    url(r"evt_master/", include(evt_master_urls)),
    url(r"evt_note/", include(evt_note_urls)),
]
