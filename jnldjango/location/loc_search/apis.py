from base.apis import MyListAPIView
from general.models import GnlLookUpTable
from rest_framework.serializers import (
    CharField,
    FloatField,
    IntegerField,
    Serializer,
)

import requests


class LocSearchSerializer(Serializer):
    name = CharField()
    result_type = CharField()

    latitude = FloatField()
    longitude = FloatField()
    distance = IntegerField()

    def to_repesentation(self, data):
        return super(LocSearchSerializer, self).to_repesentation(data)

    def __init__(self, *args, **kwargs):
        kwargs.pop("is_listview")
        return super(LocSearchSerializer, self).__init__(*args, **kwargs)


class LocSearchListAPIView(MyListAPIView):
    serializer_class = LocSearchSerializer

    def filter_queryset(self, queryset):
        return queryset

    def get_query_url(self):
        GROUP = "LOCATION_SEARCH"
        APP_CODE = GnlLookUpTable.get_lookup_value(group=GROUP, key="APP_CODE")
        APP_ID = GnlLookUpTable.get_lookup_value(group=GROUP, key="APP_ID")

        URL_TEMPLATE = "https://places.cit.api.here.com/places/v1/autosuggest" +\
            "?at={},{}&q={}&app_id={}&app_code={}"

        # latitude, longitude = 22.4904029,  114.1407555
        # q = "food"
        latitude = self.request.query_params.get("latitude")
        longitude = self.request.query_params.get("longitude")
        q = self.request.query_params.get("q")
        return URL_TEMPLATE.format(latitude, longitude, q, APP_ID, APP_CODE)

    def request_to_search_engine(self, url):
        res = requests.get(url)
        status_code = int(res.status_code)
        if status_code >= 200 and status_code < 300:
            return res.json()["results"]
        else:
            return []

    def modify_query_result(self, results):
        modified_results = results
        for modified_result in modified_results:
            position = modified_result.get("position", [-91.0,  -181.0])
            modified_result["name"] = modified_result.get("title", "")
            modified_result["latitude"] = position[0]
            modified_result["longitude"] = position[1]
            modified_result["result_type"] = modified_result["resultType"]
            modified_result["distance"] = modified_result.get("distance", -1)
        return modified_results

    def get_queryset(self):
        url = self.get_query_url()
        results = self.request_to_search_engine(url)
        return self.modify_query_result(results)
