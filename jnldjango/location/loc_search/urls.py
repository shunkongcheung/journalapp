from django.conf.urls import url

from .apis import (
    LocSearchListAPIView,
)

urlpatterns = [
    url(r"^list/$", LocSearchListAPIView.as_view()),
]
