from django.conf.urls import url

from .apis import (
    LocMasterCreateAPIView,
    LocMasterListAPIView,
    LocMasterObjectAPIView,
)

urlpatterns = [
    url(r"^create/$", LocMasterCreateAPIView.as_view()),
    url(r"^list/$", LocMasterListAPIView.as_view()),
    url(r"^(?P<pk>[0-9]+)/$", LocMasterObjectAPIView.as_view()),
]
