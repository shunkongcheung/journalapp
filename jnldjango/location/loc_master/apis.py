from base.apis import (
    MyCreateAPIView,
    MyListAPIView,
    MyObjectAPIView,
)
from base.serializers import MyModelSerializer
from django.utils import timezone
from location.models import LocMaster


class LocMasterSerializer(MyModelSerializer):
    class Meta:
        fields = ["name", "latitude", "longitude", "budgets"]
        model = LocMaster
        read_only_fields = ["budgets"]

    def create(self, validated_data):
        loc_master_query = LocMaster.objects.filter(
            longitude=validated_data["longitude"],
            latitude=validated_data["latitude"],
            created_by=validated_data["created_by"],
            enable=True
        )
        if loc_master_query.count():
            validated_data["modified_at"] = timezone.now()
            loc_master_query.update(**validated_data)
            return loc_master_query[0]
        else:
            return LocMaster.objects.create(**validated_data)


class LocMasterCreateAPIView(MyCreateAPIView):
    model = LocMaster
    serializer_class = LocMasterSerializer


class LocMasterListAPIView(MyListAPIView):
    model = LocMaster
    serializer_class = LocMasterSerializer


class LocMasterObjectAPIView(MyObjectAPIView):
    model = LocMaster
    serializer_class = LocMasterSerializer
