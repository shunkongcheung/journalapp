from base.models import MyBaseModel
from django.db import models

# Create your models here.


class LocMaster(MyBaseModel):
    name = models.CharField(max_length=256)
    latitude = models.FloatField()
    longitude = models.FloatField()

    class Meta():
        unique_together = (("latitude", "longitude"))

    def __str__(self):
        return self.name
