from base.admin import MyBaseAdmin
from django.contrib import admin

from .models import LocMaster


class LocMasterAdmin(MyBaseAdmin):
    list_display = ["name", "latitude", "longitude"]
    search_fields = ["name"]


# Register your models here.
admin.site.register(LocMaster, LocMasterAdmin)
