from django.conf.urls import url, include

from .loc_master import urls as loc_master_urls
from .loc_search import urls as loc_search_urls

app_name = "location"

urlpatterns = [
    url(r"loc_master/", include(loc_master_urls)),
    url(r"loc_search/", include(loc_search_urls)),
]
